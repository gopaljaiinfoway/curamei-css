import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProviderLeftSidebarComponent } from './provider-left-sidebar.component';


describe('AuthLayoutComponent', () => {
  let component: ProviderLeftSidebarComponent;
  let fixture: ComponentFixture<ProviderLeftSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProviderLeftSidebarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderLeftSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
