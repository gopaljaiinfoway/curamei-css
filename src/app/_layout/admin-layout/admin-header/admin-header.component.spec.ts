import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { adminHeaderComponent } from './admin-header.component';


describe('AuthLayoutComponent', () => {
  let component: adminHeaderComponent;
  let fixture: ComponentFixture<adminHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [adminHeaderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(adminHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
