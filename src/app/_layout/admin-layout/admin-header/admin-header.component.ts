import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.css']
})
export class adminHeaderComponent implements OnInit {

  userobj:any;
  userName:any;
  constructor(private router: Router,private cookieService: CookieService) { }

  ngOnInit(): void {
   // this.sidebarCollapse();
    this.userobj = JSON.parse(localStorage.getItem('user'));
    this.userName = this.userobj.fullName;
  }
  sidebarCollapse() {
    $('#sidebar').toggleClass('active');
    $('#body').toggleClass('active');
}
logout(){
  localStorage.removeItem('user');
  localStorage.clear();
  this.cookieService.delete('token');
  this.cookieService.deleteAll();
  this.router.navigateByUrl('');
  }
}
