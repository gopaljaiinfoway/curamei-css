import { MediaMatcher } from '@angular/cdk/layout'
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { CookieService } from 'ngx-cookie-service'
import { animateText, onMainContentChange, onSideNavChange } from '../animation'
declare var $: any
@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css'],
  animations: [onSideNavChange, animateText, onMainContentChange],
})
export class AdminLayoutComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList
  events: string[] = []
  panelOpenState: boolean
  onSideNavChange: any
  private _mobileQueryListener: () => void
  public sideNavState: boolean = false
  public linkText: boolean = false
  flag = true
  visible = 'visible'
  hidden = 'hidden'
  userobj: any
  userName: any

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router: Router,
    private cookieService: CookieService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)')
    this._mobileQueryListener = () => changeDetectorRef.detectChanges()
    this.mobileQuery.addListener(this._mobileQueryListener)
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener)
  }
  ngOnInit(): void {
    // this.sidebarCollapse();
    this.flag = true
    this.userobj = JSON.parse(localStorage.getItem('user'))
    this.userName = this.userobj.fullName
  }

  onSinenavToggle() {
    this.sideNavState = !this.sideNavState

    setTimeout(() => {
      this.linkText = this.sideNavState
    }, 200)
    // this._sidenavService.sideNavState$.next(this.sideNavState)
  }

  logout() {
    localStorage.removeItem('user')
    localStorage.clear()
    this.cookieService.delete('token')
    this.cookieService.deleteAll()
    this.router.navigateByUrl('')
  }
}
