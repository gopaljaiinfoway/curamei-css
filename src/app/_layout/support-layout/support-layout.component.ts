import { MediaMatcher } from '@angular/cdk/layout'
import { ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { MatSidenav } from '@angular/material/sidenav'
import { Router } from '@angular/router'
import { CookieService } from 'ngx-cookie-service'
import { BehaviorSubject } from 'rxjs'
import jwt_decode from 'jwt-decode';
import { environment } from '../../../environments/environment';
import { animateText, onMainContentChange, onSideNavChange } from '../animation';
import { CommmanService } from '../../_services/commman.service';
declare var $: any

interface Page {
  link: string
  name: string
  icon: string
}

@Component({
  selector: 'app-support-layout',
  templateUrl: './support-layout.component.html',
  styleUrls: ['./support-layout.component.css'],
  animations: [onSideNavChange, animateText, onMainContentChange],
})
export class SupportLayoutComponent implements OnInit {

  mobileQuery: MediaQueryList
  events: string[] = []
  panelOpenState: boolean
  onSideNavChange: any
  private _mobileQueryListener: () => void
  public sideNavState: boolean = false
  public linkText: boolean = false
  flag = true
  visible = 'visible'
  hidden = 'hidden'
  userobj: any
  userName: any
  screenWidth: number;
  userTypeId:any;
  medicalRecordsShow :boolean=false;
  usr_id: any;
  basePath: any;
  private screenWidth$ = new BehaviorSubject<number>(window.innerWidth);
  @ViewChild('sidenav') sidenav: MatSidenav;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth$.next(event.target.innerWidth);
  }
  constructor ( 
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router: Router,
    private cookieService: CookieService,
    private commanService: CommmanService
  ) {
    this.basePath = environment.apiBaseUrl;
    let tokenGet = this.cookieService.get('token');
    if (tokenGet && tokenGet != null) {
      let decoded = jwt_decode(tokenGet, 'llp');
      this.userTypeId = decoded.userTypeId;
      this.usr_id = decoded.id;
    }

    this.mobileQuery = media.matchMedia('(max-width: 600px)')
    this._mobileQueryListener = () => changeDetectorRef.detectChanges()
    this.mobileQuery.addListener(this._mobileQueryListener)
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener)
  }
  ngOnInit(): void {
    // this.sidebarCollapse();
    this.medicalRecordsEnable();
    this.flag = true
    this.userobj = JSON.parse(localStorage.getItem('user'))
    this.userName = this.userobj.fullName;
    this.screenWidth$.subscribe(width => {
      this.screenWidth = width;
    });
  }

  onSinenavToggle() {
    this.sideNavState = !this.sideNavState

    setTimeout(() => {
      this.linkText = this.sideNavState
    }, 200)
    // this._sidenavService.sideNavState$.next(this.sideNavState)
  }

  close() {
    console.log('escape clicked')
    this.sideNavState = false;

  }

  logout() {
    localStorage.removeItem('user')
    localStorage.clear()
    this.cookieService.delete('token')
    this.cookieService.deleteAll()
    this.router.navigateByUrl('')
  }

  //   sidebarCollapse() {
  //     $("#sidebar").toggleClass('active');
  //     $('#body').toggleClass('active');
  // }


  medicalRecordsEnable() {
    this.commanService.checkmedicalRecordsAccess().subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.medicalRecordsShow =true;
        }else{
          this.medicalRecordsShow =false;
        } 
      },
      (err) => {
        console.log(err);
      }
    );
  }

  goToDashboard() {
    this.router.navigateByUrl('/support/dashboard')
  }
  profilePage(){
    this.router.navigateByUrl('/support/support-profile') 
  }
}
