import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-patient-header',
  templateUrl: './patient-header.component.html',
  styleUrls: ['./patient-header.component.css']
})
export class PatientHeaderComponent implements OnInit {
  userobj:any;
  userName:any;
  constructor(private router: Router,private cookieService: CookieService) { }

  ngOnInit(): void {
   // this.sidebarCollapse();
    this.userobj = JSON.parse(localStorage.getItem('user'));
    this.userName = this.userobj.fullName;
  }
  sidebarCollapse() {
    $('#sidebar').toggleClass('active');
    $('#body').toggleClass('active');
}
logout(){
  localStorage.removeItem('user');
  localStorage.clear();
  this.cookieService.delete('token');
  this.cookieService.deleteAll();
  this.router.navigateByUrl('');
  }
}
