import { MediaMatcher } from '@angular/cdk/layout'
import { ChangeDetectorRef, Component, HostListener, OnInit, ViewChild } from '@angular/core'
import { MatSidenav } from '@angular/material/sidenav'
import { Router } from '@angular/router'
import { CookieService } from 'ngx-cookie-service'
import { BehaviorSubject } from 'rxjs';
import * as jwtDecode from 'jwt-decode';
import { environment } from '../../../environments/environment';
import { animateText, onMainContentChange, onSideNavChange } from '../animation'
import { CommmanService } from '../../_services/commman.service';
import { SocketioService } from 'src/app/socketIO.service'

declare var $: any
@Component({
  selector: 'app-patient-layout',
  templateUrl: './patient-layout.component.html',
  styleUrls: ['./patient-layout.component.css'],
  animations: [onSideNavChange, animateText, onMainContentChange],
})
export class PatientLayoutComponent implements OnInit {

  socket: SocketIOClient.Socket;



  notificationArr = [];
  notificationLength = 0;
  mobileQuery: MediaQueryList
  events: string[] = []
  panelOpenState: boolean
  onSideNavChange: any
  private _mobileQueryListener: () => void
  public sideNavState: boolean = false
  public linkText: boolean = false
  flag = true
  visible = 'visible'
  hidden = 'hidden'
  userobj: any
  userName: any
  screenWidth: number;
  usr_id: any;
  basePath: any;
  fitbitConfigurationFlag: boolean=false;
  private screenWidth$ = new BehaviorSubject<number>(window.innerWidth);
  @ViewChild('sidenav') sidenav: MatSidenav;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth$.next(event.target.innerWidth);
  }

  constructor (
    private socketService: SocketioService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router: Router,
    private cookieService: CookieService,
    private commonService: CommmanService,
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)')
    this._mobileQueryListener = () => changeDetectorRef.detectChanges()
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.basePath = environment.apiBaseUrl;
    var decoded = jwtDecode(this.cookieService.get('token'));
    this.usr_id = decoded.id;
  }

  ngOnDestroy(): void {
    console.log('===destroyed===');
    this.getNotification()
    this.mobileQuery.removeListener(this._mobileQueryListener)
  }
  ngOnInit(): void {
    // this.backdropValue = false;
     this.checkUserConfigured();
    this.flag = true
    this.userobj = JSON.parse(localStorage.getItem('user'))
    this.userName = this.userobj.fullName
    this.screenWidth$.subscribe(width => {
      this.screenWidth = width;
    });
    this.getNotification()
    this.socketService.getData()
    .subscribe((result: any) =>{
      if(result.flag){
        this.getNotification()
      }
    } );
    
  }
  getNotification(){
    this.commonService.getNotification()
    .subscribe((result: any) => {
      if(result.success){
        this.notificationArr = result.notification;
        this.notificationLength = result.notification.length;
      }
    })
  }
  makeUnreadNotification(){
    console.log(this.notificationArr);
    this.commonService.unreadNotification(this.notificationArr)
    .subscribe((result: any) => {
      this.notificationLength = 0;
      
    })
  }
  onSinenavToggle() {
    this.sideNavState = !this.sideNavState
    // this.backdropValue = true;
    setTimeout(() => {

      this.linkText = this.sideNavState
    }, 200)
    // this._sidenavService.sideNavState$.next(this.sideNavState)
  }

  logout() {
    localStorage.removeItem('user')
    localStorage.clear()
    this.cookieService.delete('token')
    this.cookieService.deleteAll()
    this.router.navigateByUrl('')
  }
  goToDashboard() {
    this.router.navigateByUrl('/patient/dashboard')
  }
  profilePage(){
    this.router.navigateByUrl('/patient/patient-profile') 
  }
  checkUserConfigured() {
    this.commonService.checkFitbitConfig().subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.fitbitConfigurationFlag=true;
          localStorage.setItem('fitbit_refresh_token', resmsg.data);
        }else{
          this.fitbitConfigurationFlag=false;
        }
        
      },
      (err) => {
        this.fitbitConfigurationFlag=false;
      }
    );
  }
}
