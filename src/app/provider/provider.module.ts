import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderRoutingModule } from './provider-routing.module';
import { ProviderDashboardComponent } from './provider-dashboard/provider-dashboard.component';
import { PatientMedicalViewComponent } from './patient-medical-view/patient-medical-view.component';
import { ManageApplicationPermissionComponent } from './manage-application-permission/manage-application-permission.component';
import { ProviderFitbitDashboardComponent } from './provider-fitbit-dashboard/provider-fitbit-dashboard.component';
import { ProviderProfileComponent } from './provider-profile/provider-profile.component';
import { ManagePatientAccessComponent } from './manage-patient-access/manage-patient-access.component';


@NgModule({
  declarations: [ProviderDashboardComponent, PatientMedicalViewComponent, ManageApplicationPermissionComponent, ProviderFitbitDashboardComponent, ProviderProfileComponent, ManagePatientAccessComponent],
  imports: [
    CommonModule,
    ProviderRoutingModule
  ]
})
export class ProviderModule { }
