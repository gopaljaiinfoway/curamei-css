import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as jwtDecode from 'jwt-decode';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../../environments/environment';
import { CommmanService } from '../../_services/commman.service';
import { Router } from '@angular/router';
 
@Component({
  selector: 'app-provider-profile',
  templateUrl: './provider-profile.component.html',
  styleUrls: ['./provider-profile.component.css']
})
export class ProviderProfileComponent implements OnInit {


  userProfileForm: FormGroup;
  usaStateList: Object;
  user_fn: any;
  action: any = '';
  updateBtn = 'Update';
  previewFlag = false;
  fileData: File = null;
  previewUrl: any = "https://via.placeholder.com/100x120";
  uploadedFilePath: string = null;
  imagePath: any;
  basePath: any;
  usr_id: any;
  userTypeId:any;
  systemId:boolean=false;
  imageUploadBtn='Upload';
  constructor (
    private commanService: CommmanService,
    private cookieService: CookieService,
    private _snackBar: MatSnackBar,
    private http: HttpClient,
    private router: Router
  ) {
    var decoded = jwtDecode(this.cookieService.get('token'));
    this.usr_id = decoded.id;
    this.userTypeId = decoded.userTypeId;
    this.basePath = environment.apiBaseUrl;
    this.userProfileForm = new FormGroup({
      first_name: new FormControl('',Validators.required),
      last_name: new FormControl('',Validators.required),
      username: new FormControl('',Validators.required),
      id: new FormControl('',Validators.required),
      imageUrl: new FormControl(''),
      email: new FormControl('',[Validators.required,Validators.email]),
      phone_number: new FormControl('',[Validators.required,Validators.maxLength(12)]),
      npi: new FormControl({value: '', disabled: true}),
      address1: new FormControl('',[Validators.required]),
      address2: new FormControl(''),
      state_id: new FormControl('',[Validators.required]),
      zipcode: new FormControl('8456321',[Validators.required]),
      fax_number: new FormControl('8456321',[Validators.required]),
    });
  }

  ngOnInit(): void {
    this.getUserData();

    this.commanService
      .getUsaStateList()
      .subscribe((data) => (this.usaStateList = data));
  }
  addUser() {
    this.updateBtn = 'Loading...';
    console.log(this.userProfileForm.value)
    this.commanService.updateProviderProfile(this.userProfileForm.value).subscribe(
      (res: any) => {
        console.log(res.success);
        this.updateBtn = 'Update';
        if (res.success) {
          this._snackBar.open(res.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['green-snackbar'],
          });
        }
      },
      (err) => {
        console.log('Profile Not Updated');
      }
    );
  }



  getUserData() {
    this.commanService.getUserProfileList(this.userTypeId).subscribe(
      (res: any) => {
        console.log("===========get provider profile data======================"+this.userTypeId);
        console.log(res.data);
        console.log("===========get provider profile data======================"+this.userTypeId);
        if (res.success) {
          this.userProfileForm.patchValue(res.data);
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  fileProgress(fileInput: any) {
    this.previewFlag = true;
    this.fileData = <File>fileInput.target.files[0];
    this.preview();

  }

  preview() {
    // Show preview
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    }
  }

  upload() {
    const formData: any = new FormData();
    const file = this.fileData;
    if (file == null) {
      this._snackBar.open('Please select photo', this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['red-snackbar'],
      });
    } else {
      formData.append("upload", file);
    }
    this.imageUploadBtn = 'Uploading...';
    this.commanService.uploadProfileImage(formData).subscribe((result: any) => {
      if (result.success) {
        this.imageUploadBtn='Upload';
        this.router.navigate(['/provider/profile']);
        this._snackBar.open('File Uploaded successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
        
      }
      else {
        this.imageUploadBtn='Upload';
        console.log('File Not Uploaded ')
      }
    })
  }

}
