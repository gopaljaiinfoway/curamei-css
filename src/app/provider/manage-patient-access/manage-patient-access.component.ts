import { Component, OnInit } from '@angular/core';
import { CommmanService } from 'src/app/_services/commman.service';
import { MatSnackBar } from '@angular/material/snack-bar'
import { FormControl, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-manage-patient-access',
  templateUrl: './manage-patient-access.component.html',
  styleUrls: ['./manage-patient-access.component.css']
})
export class ManagePatientAccessComponent implements OnInit {

  users: any;
  user_id: any;
  user_type_id: any;
  AceeptLink = 'Accept';
  rejectLink = 'Reject';
  AceeptedLink='Accepted';
  rejectedLink = 'Rejected';
  action=null;
  searchType:any;
  searchFieldFlag:boolean=false;
  placholdeName="Search By First Name";
  searchDataValue:any;
  searchFilter:any;
  constructor(
    private commanService: CommmanService,    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.user_type_id = 2;
       this.getAllPatient();
       this.searchType=[{name:'E-mail',value:'email'},{name:'Name',value:'name'}];
  }
   // All Patient
    getAllPatient(){
      this.commanService.getPatientListByType(this.user_type_id)
      .subscribe((result: any) => {
        if(result.success){
          this.users = result.data;
        }
        else
        this.users=[];
      })
    }
    selectForRecord(type){
      this.searchDataValue='';
      if(type == 0){
        this.user_type_id = 2;
        this.getAllPatient();
      }
      if(type == 1){
        this.user_type_id = 3;
        this.getAllPatient();
      }
    }
    providerManageAccessUser(upid,status){
      this.commanService.providerManageAccessUser({upid:upid,status:status})
      .subscribe((res: any) => {
         if(res.success){
          this.getAllPatient();
          this._snackBar.open(res.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['green-snackbar'],
          });
         }else{
          this._snackBar.open(res.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
         }
       
      },err =>{

      })
    }

    selectedSearchType(filter){
        if(filter!=undefined){
          this.searchFieldFlag=true;
          this.searchFilter = filter;
                  if(filter=='email')
                    this.placholdeName="Search By E-mail";
                    else
                    this.placholdeName="Search By First Name";
        }else{
          this.searchFieldFlag=false;
          this.placholdeName="Search By First Name";
        }
    }

    getSearchByData(){
       if(this.searchDataValue==undefined || this.searchDataValue=='')
       this.getAllPatient();
       else{
              this.commanService.getPatientListByTypeWithFilter(this.searchFilter,this.user_type_id,this.searchDataValue)
              .subscribe((result: any) => {
               // this.searchDataValue='';
                if(result.success){
                  this.users = result.data;
                }
                else
                this.users=[];
              })
       }
    }

}
