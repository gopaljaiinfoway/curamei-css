import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-provider-dashboard',
  templateUrl: './provider-dashboard.component.html',
  styleUrls: ['./provider-dashboard.component.css']
})
export class ProviderDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.sidebarCollapse();
  }
  sidebarCollapse() {
    $('#sidebar').toggleClass('active');
    $('#body').toggleClass('active');
}

}
