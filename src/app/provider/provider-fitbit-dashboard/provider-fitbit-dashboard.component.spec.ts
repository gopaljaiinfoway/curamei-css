import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderFitbitDashboardComponent } from './provider-fitbit-dashboard.component';

describe('ProviderFitbitDashboardComponent', () => {
  let component: ProviderFitbitDashboardComponent;
  let fixture: ComponentFixture<ProviderFitbitDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderFitbitDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderFitbitDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
