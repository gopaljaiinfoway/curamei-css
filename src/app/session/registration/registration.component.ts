import { ChangeDetectorRef, Component, OnInit } from '@angular/core'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import jwt_decode from 'jwt-decode'
import { CustomValidators } from 'ng2-validation'
import { CookieService } from 'ngx-cookie-service'
import { CommmanService } from '../../_services/commman.service';
import { MatSnackBar } from '@angular/material/snack-bar'
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {
  hide = true
  hideTwo = true
  buttonText = 'Sign Up'
  eEmail = false
  pEmailFlag = false
  eContact = false
  eNpi = false
  result: any
  results: any
  npidata: any
  npidetails: any
  firstname: any
  lastname: any
  npinumber: any
  public signUpForm: FormGroup
  serverErrorMessages: any
  taxocode: any
  taxostate: any
  taxolicense: any
  showForm = false
  supportForm = false
  errorNpi: boolean = false
  selectedType = ''
  usaStateList: Object
  showStateName: boolean = false
  displayProviderForm: boolean = false
  displayPatientForm: boolean = false
  showSignUpType: boolean = true
  phonenumPattern = '^((\\+1-?)|0)?[0-9]{10}$'
  hideModal: boolean = false
  openModalSubmit: any = true
  hideErrorMsg = false
  npi: any
  apiDataError: string
  showApiError: boolean = false
  displaySupportForm: boolean = false
  eProviderEmail: boolean = false
  showSupportField: boolean = false
  errorMessageProviderEmail = 'Invalid provider email'
  npiBox: boolean
  npirecords: any
  taxorecord: any
  filterTaxoRecord: any
  npiInputCheck: boolean = true
  formStepOne: boolean
  formStepTwo: boolean
  formStepThree: boolean
  termservicevalue: boolean = false;
  trueValue: boolean = true;
  falseValue: boolean = false;
  cvalue: any;
  checked: boolean = false;
  action = null
  constructor (
    private formBuilder: FormBuilder,
    private commanService: CommmanService,
    private cookieService: CookieService,
    private router: Router,
    private cdr: ChangeDetectorRef,
    private _snackBar: MatSnackBar,
  ) {
    let pass = new FormControl('', Validators.required)
    let cpass = new FormControl('', CustomValidators.equalTo(pass))
    this.signUpForm = new FormGroup({
      sType: new FormControl('', Validators.required),
      fname: new FormControl('', Validators.required),
      lname: new FormControl(''),
      npi: new FormControl('', Validators.maxLength(10)),
      emailid: new FormControl('', [Validators.required, CustomValidators.email]),
      phoneone: new FormControl('', [
        Validators.required,
        Validators.maxLength(3),
        Validators.minLength(3),
        CustomValidators.number,
      ]),
      phonetwo: new FormControl('', [
        Validators.required,
        Validators.maxLength(3),
        Validators.minLength(3),
        CustomValidators.number,
      ]),
      phonethree: new FormControl('', [
        Validators.required,
        Validators.maxLength(4),
        CustomValidators.number,
      ]),
      addressone: new FormControl(''),
      addresstwo: new FormControl(''),
      faxone: new FormControl('', [
        Validators.maxLength(3),
        Validators.minLength(3),
        CustomValidators.number,
      ]),
      faxtwo: new FormControl('', [
        Validators.maxLength(3),
        Validators.minLength(3),
        CustomValidators.number,
      ]),
      faxthree: new FormControl('', [
        Validators.maxLength(4),
        Validators.minLength(4),
        CustomValidators.number,
      ]),
      stateid: new FormControl(''),
      zipcode: new FormControl('', [
        Validators.maxLength(10),
        Validators.minLength(9),
        CustomValidators.number,
      ]),
      pemail: new FormControl('', [CustomValidators.email]),
      termsofservices: new FormControl('', [Validators.required]),

      pass: pass,
      cpass: cpass,
    })
  }

  ngOnInit(): void {
    this.commanService.getUsaStateList().subscribe((data) => (this.usaStateList = data))
  }

  check() {
    this.termservicevalue = true;
  }

  changeValue(value) {
    this.checked = !value;
    console.log(this.checked)
  }

  onSubmit() {
    this.buttonText = 'Loading....'
    console.log(this.signUpForm.value)
    if (!this.signUpForm.invalid) {
      this.commanService.userSignUp(this.signUpForm.value).subscribe(
        (res: any) => {
          if (res.success) {
            this.router.navigateByUrl('signup-success')
            this.buttonText = 'Sign Up'
          }else{
            this._snackBar.open(res.message, this.action, {
              duration: 3000,
              verticalPosition: 'top', // 'top' | 'bottom'
              panelClass: ['red-snackbar'],
            });
          }
        },
        (err) => {
          this.buttonText = 'Sign Up'
          this.serverErrorMessages = err.error.message
        }
      )
    }else{
    }
  }

  checkEmail(emailValue) {
    if (emailValue !== undefined) {
      if (emailValue.length > 0) {
        this.commanService.emailCheckExists(emailValue).subscribe((result: any) => {
          console.log(result)
          if (result.success == true) {
            this.eEmail = true
          } else {
            this.eEmail = false
          }
        })
      }
    }
  }

  checkContact(contactValue) {
    if (contactValue !== undefined) {
      if (contactValue.length > 0) {
        this.commanService.ContactCheckExists(contactValue).subscribe((result: any) => {
          console.log(result)
          if (result.success == true) {
            this.eContact = true
          } else {
            this.eContact = false
          }
        })
      }
    }
  }

  checkProviderEmail(providerEmailValue) {
    console.log('focusout')
    if (providerEmailValue !== undefined) {
      if (providerEmailValue.length > 0) {
        this.commanService.userExists(providerEmailValue).subscribe((result: any) => {
          console.log(result)
          if (result.success == true) {
            this.eProviderEmail = false
          } else {
            this.eProviderEmail = true;
          }
        })
      }
    }
  }

  checkNpi(npi) {
    if (npi !== undefined) {
      if (npi.length > 0) {
        this.commanService.NpiCheckExists(npi).subscribe((result: any) => {
          console.log(result)
          if (result.success == true) {
            this.eNpi = true
            this.showApiError = true
            this.apiDataError = result.message
          } else {
            // this.eNpi = false;

            this.commanService.getnpi(npi).subscribe((data) => {
              this.npidata = data
              this.eNpi = false;
              this.npiBox = true
              this.npiInputCheck = false;
              const objects = Object.keys(data).map((key) => data[key])
              this.npirecords = objects[1]
              console.log(this.npirecords.length)
              if (this.npirecords.length == 0) {
                console.log(this.npirecords)
                this.npiInputCheck = true;
                this.npiBox = false;
                this.showApiError = true
                this.apiDataError = 'Invalid NPI'
              }
              else {
                console.log('not empty')
                this.npirecords.forEach((record) => {
                  if (record.enumeration_type === 'NPI-1') {
                    console.log('******************************')
                    console.log(record)
                    this.npinumber = record.number
                    this.firstname = record.basic.first_name
                    this.lastname = record.basic.last_name
                    this.taxorecord = record.taxonomies
                    this.taxorecord.forEach((record) => {
                      if (record.primary) {
                        console.log('---------------------------------')
                        this.filterTaxoRecord = record
                        console.log(this.filterTaxoRecord)
                        console.log('---------------------------------')
                      }
                    })
                    console.log('******************************')
                  } else if (record.enumeration_type === 'NPI-2') {
                    console.log('Not a valid NPI no.')
                    this.npiInputCheck = true;
                    this.npiBox = false;
                    this.showApiError = true
                    this.apiDataError = 'Invalid NPI'
                  }
                })
              }
            })
          }
        })
      }
    }
  }

  onChange(event) {
    this.selectedType = event.target.value
    console.log(this.selectedType)
  }


  fillInfo() { }

  ngAfterViewChecked() {
    this.cdr.detectChanges()
  }



  hideForm() {
    this.showForm = false
    console.log('clicked' + this.showForm)
  }

  // displayForm() {
  //   this.showForm = true
  //   this.displayProviderForm = false
  // }

  // selectSignUpForm() {
  //   if (this.selectedType === '1') {
  //     this.displayProviderForm = true
  //     this.showSignUpType = false
  //   } else if (this.selectedType === '2') {
  //     this.displayPatientForm = true
  //     this.showSignUpType = false
  //   } else {
  //     this.displaySupportForm = true
  //     this.showSignUpType = false
  //   }
  // }







  hoverState() {
    this.showStateName = true
  }



  checkError() {
    this.eNpi = false
    this.showApiError = false
    console.log('focus in ' + this.hideErrorMsg)
  }

  gobacktoNpiBox() {
    this.npiInputCheck = true;
    this.npiBox = false;
  }

  companyNameClick() {
    var tokenGet = this.cookieService.get('token')
    if (tokenGet && tokenGet != null) {
      let decoded = jwt_decode(tokenGet, 'llp')
      if (decoded.userTypeId === 1) {
        this.router.navigateByUrl('/provider/dashboard')
      } else if (decoded.userTypeId === 2) {
        if (decoded.enrollment_flg) {
          this.router.navigateByUrl('/patient/dashboard')
        } else {
          this.router.navigateByUrl('/patient/enrollment')
        }
      } else if (decoded.userTypeId === 4) {
        this.router.navigateByUrl('/admin/dashboard')
      }
    } else {
      this.router.navigateByUrl('/signin')
    }
  }

  displayForm() {
    this.npiBox = false
    this.showForm = true

    this.formStepOne = true
    this.formStepTwo = false
    // this.displayProviderForm = false
  }

  gotoFormStepTwo() {
    this.formStepOne = false
    this.formStepTwo = true
  }

  goBackStepNpiForm() {
    this.npiInputCheck = true
    this.npiBox = false
    this.showForm = false
    this.formStepOne = false
  }

  goBacktopStepOne() {
    this.formStepOne = true
    this.formStepTwo = false
  }

  goBacktopStepTwo() {
    this.formStepTwo = true
    this.formStepThree = false
  }

  gotoFormStepThree() {
    this.formStepTwo = false
    this.formStepThree = true
  }

  selectSignUpForm() {
    if (this.selectedType === '1') {
      this.displayProviderForm = true
      this.showSignUpType = false
    } else if (this.selectedType === '2') {
      this.displayPatientForm = true
      this.showSignUpType = false
      this.formStepOne = true
    } else {
      this.displaySupportForm = true
      this.showSignUpType = false
      this.formStepOne = true
    }
  }

  selectUserForm() {
    if (this.selectedType === '1') {
      this.displayProviderForm = true
      this.showSignUpType = false
    } else if (this.selectedType === '2') {
      this.displayPatientForm = true
      this.showSignUpType = false
    } else if (this.selectedType === '3') {
      this.displaySupportForm = true
      this.showSignUpType = false
    } else {
      this.displayProviderForm = false
      this.displaySupportForm = false
      this.displaySupportForm = false
      this.showSignUpType = true
    }
  }
}
