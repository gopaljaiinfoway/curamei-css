import { HttpClientModule } from '@angular/common/http';
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularMaterialModule } from 'src/app/material-module';
import { CommmanService } from './../../_services/commman.service';
import { LoginComponent } from './login.component';


fdescribe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [ReactiveFormsModule, FormsModule, RouterTestingModule, HttpClientModule, AngularMaterialModule, BrowserAnimationsModule],
      providers: [CommmanService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('[Component-Check]- should create Login component', () => {
    expect(component).toBeTruthy();

  });

  it('[Username-Check] - should check users username is invalid', () => {
    let name = component.loginForm.controls['username'];
    expect(name.valid).toBeFalsy();
    expect(name.pristine).toBeTruthy();
    expect(name.errors['required']).toBeTruthy();
    name.setValue('abc');
  })

  it('[Username-Check] - should check users correct username is entered', () => {
    let name = component.loginForm.controls['username'];
    name.setValue('john')
    expect(name.errors).toBeNull();
  })

  it('[Password-Check] - should check passwords errors', () => {
    let pwd = component.loginForm.controls['password'];
    expect(pwd.errors['required']).toBeTruthy();

  })

  it('[Password-Check] - should check passwords is entered', () => {
    let pwd = component.loginForm.controls['password'];
    pwd.setValue('abc@123')
    expect(pwd.errors).toBeNull();

  })

  it('[Form-Check] - should check form is invalid or not if no values entered', () => {
    expect(component.loginForm.valid).toBeFalsy();
  })

  it('[Form-Check] - should check form is valid or not when values are entered', () => {
    component.loginForm.controls['username'].setValue('abcd');
    component.loginForm.controls['password'].setValue('abc@123');
    expect(component.loginForm.valid).toBeTruthy();
  })

  it('[Form-Submit] - should check form is submitted', fakeAsync(() => {
    component.loginForm.controls['username'].setValue('abcd');
    component.loginForm.controls['password'].setValue('abc@123');
    spyOn(component, 'onSubmit');

    const el = fixture.debugElement.query(By.css('.login-button')).nativeElement.click();
    expect(component.onSubmit).toHaveBeenCalled();
    fixture.detectChanges();
  }))

  it('[Login-Service-Check] - should call login method', async(() => {
    let loginElement: DebugElement;
    let debugElement = fixture.debugElement;
    let commonService = debugElement.injector.get(CommmanService);
    let loginSpy = spyOn(commonService, 'login').and.callThrough();
    loginElement = fixture.debugElement.query(By.css('.login-button'));
    component.loginForm.controls['username'].setValue('abcd');
    component.loginForm.controls['password'].setValue('abc@123');
    loginElement.nativeElement.click();
    expect(loginSpy).toHaveBeenCalledTimes(1);
  }));

});
