import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import jwt_decode from 'jwt-decode'
import { CookieService } from 'ngx-cookie-service'
import { CommmanService } from '../../_services/commman.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  hide = true
  buttonText = 'Sign In'
  userData: any
  serverErrorMessages: any
  public loginForm: FormGroup
  errorStatus = false
  errorMessage = ''
  constructor (
    private formBuilder: FormBuilder,
    private commanService: CommmanService,
    private cookieService: CookieService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
    })
  }

  onSubmit() {
    this.buttonText = 'Sending...'
    this.commanService.login(this.loginForm.value).subscribe(
      (resmsg: any) => {
        this.buttonText = 'Sign In'
        if (resmsg.success == true) {
          var expire = new Date()
          var time = Date.now() + 3600 * 1000 * 6 // current time + 6 hours ///
          expire.setTime(time)
          this.cookieService.set('token', resmsg.data.token, expire)
          this.userData = {
            fullName: resmsg.data.fullName,
            last_login: resmsg.data.last_login,
            oneup_user_id: resmsg.data.oneup_user_id
          }
          localStorage.setItem('user', JSON.stringify(this.userData));
          if (resmsg.data.userTypeId === 1) {
            this.router.navigateByUrl('/provider/dashboard')
          } else if (resmsg.data.userTypeId === 2) {
            if (resmsg.data.enrollment_flg) {
              this.router.navigateByUrl('/patient/dashboard')
            } else {
              this.router.navigateByUrl('/patient/enrollment')
            }
          } else if (resmsg.data.userTypeId === 3) {
            this.router.navigateByUrl('/support/dashboard')
          } 
          else if (resmsg.data.userTypeId === 4) {
            this.router.navigateByUrl('/admin/dashboard')
          }
        } else {
          this.errorStatus = true
          this.errorMessage = resmsg.message
          this.buttonText = 'Sign In'
        }
      },
      (err) => {
        this.buttonText = 'Sign In'
        this.serverErrorMessages = err.error.message
      }
    )
  }

  companyNameClick() {
    let tokenGet = this.cookieService.get('token')
    if (tokenGet && tokenGet != null) {
      let decoded = jwt_decode(tokenGet, 'llp')
      if (decoded.userTypeId === 1) {
        this.router.navigateByUrl('/provider/dashboard')
      } else if (decoded.userTypeId === 2) {
        if (decoded.enrollment_flg) {
          this.router.navigateByUrl('/patient/dashboard')
        } else {
          this.router.navigateByUrl('/patient/enrollment')
        }
        
      } else if (decoded.userTypeId === 4) {
        this.router.navigateByUrl('/admin/dashboard')
      }
      else if (decoded.userTypeId === 3) {
        this.router.navigateByUrl('/support/dashboard')
      }
    } else {
      this.router.navigateByUrl('/signin')
    }
  }
}
