import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { signupVerifyComponent } from './signup-verify.component';


describe('LoginComponent', () => {
  let component: signupVerifyComponent;
  let fixture: ComponentFixture<signupVerifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [signupVerifyComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(signupVerifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
