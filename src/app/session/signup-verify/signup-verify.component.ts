import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { CommmanService} from '../../_services/commman.service';

import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';

@Component({
  selector: 'app-signup-verify',
  templateUrl: './signup-verify.component.html',
  styleUrls: ['./signup-verify.component.css']
})
export class signupVerifyComponent implements OnInit {
  params: any;
  Tinvalid=false;
  msuccess=false;
 constructor(private route: ActivatedRoute, private commanService: CommmanService){

  this.params = this.route.snapshot.queryParams["token"];
  console.log("this.params");
  console.log(this.params);
  //this.params = route.snapshot.params;
  console.log("this.params");
  }
  ngOnInit(): void {
    if (!this.params) {
      this.Tinvalid=true; 
    } else {
      this.commanService.verifyUser({'e_token':this.params}).subscribe((result:any)=>{
     if(result.success == true){
         this.msuccess = true;
    this.Tinvalid = false;
      }else{
      this.Tinvalid = true;
       this.msuccess = false;
      }
     })
    }

  }

}
