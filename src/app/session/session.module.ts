import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { LoginComponent } from './login/login.component';
import { PatientAccountSetupComponent } from './patient-account-setup/patient-account-setup.component';
import { SessionRoutingModule } from './session-routing.module';
import { TermsOfServicesComponent } from './terms-of-services/terms-of-services.component';

@NgModule({
  declarations: [LoginComponent, TermsOfServicesComponent, PatientAccountSetupComponent],
  imports: [
    CommonModule,
    SessionRoutingModule,
    BsModalService,
    ModalModule.forRoot(),
    RouterModule,
  ],
})
export class SessionModule { }
