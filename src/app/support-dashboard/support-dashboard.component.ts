import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-support-dashboard',
  templateUrl: './support-dashboard.component.html',
  styleUrls: ['./support-dashboard.component.css']
})
export class SupportDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.sidebarCollapse();
  }
  sidebarCollapse() {
    $('#sidebar').toggleClass('active');
    $('#body').toggleClass('active');
}

}
