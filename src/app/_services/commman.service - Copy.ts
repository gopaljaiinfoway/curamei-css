import { Location } from '@angular/common';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { map } from 'rxjs/operators';
import 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { UserAuthenticationCheckService } from './user-authentication-check.service';

@Injectable({
  providedIn: 'root',
})
export class CommmanService {
  headers: any;
  basePath: any;
  headersWithMultipart: any;
  constructor (
    private http: HttpClient,
    private userGetHeader: UserAuthenticationCheckService,
    private cookieService: CookieService
  ) {
    this.basePath = environment.apiBaseUrl;
    this.headers = this.userGetHeader.getHeader();
    this.headersWithMultipart = this.userGetHeader.getHeaderWithMultipart();
  }

  //===============Signup  service here start ===============

  isLoggedInUser() {
    return this.cookieService.get('token');
  }

  deleteToken() {
    return this.cookieService.delete('token');
  }

  Logout() {
    return this.http.get(environment.apiBaseUrl + 'userLogout');
  }

  login(data: any) {
    console.log(data);
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `login`), {
        observe: 'response',
        headers: this.headers,
        body: JSON.stringify(data),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  userSignUp(data: any) {
    console.log(data);
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `signup`), {
        observe: 'response',
        headers: this.headers,
        body: JSON.stringify(data),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  emailCheckExists(data: any) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `emailCheck`), {
        observe: 'response',
        headers: this.headers,
        body: JSON.stringify({ email: data }),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  ContactCheckExists(data: any) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `contactCheck`), {
        observe: 'response',
        headers: this.headers,
        body: JSON.stringify({ mobile: data }),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  NpiCheckExists(data: any) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `npiCheck`), {
        observe: 'response',
        headers: this.headers,
        body: JSON.stringify({ npi: data }),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  userExists(data: any) {
    console.log('user exists' + data);
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `checkUsers`), {
        observe: 'response',
        headers: this.headers,
        body: JSON.stringify({ pemail: data }),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  verifyUser(data: any) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `user-verify`), {
        observe: 'response',
        headers: this.headers,
        body: JSON.stringify(data),
      })
      .map((response) => {
        switch (response.status) {
          case 200: {
            return response.body;
          }
        }
      });
  }

  public getnpi(npi: number) {
    return this.http
      .request(
        'get',
        Location.joinWithSlash(`${this.basePath}`, `npi/${encodeURIComponent(npi)}`),
        {
          observe: 'response',
          headers: this.headers,
        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  getProvderFromList(id: number) {
    return this.http
      .request(
        'get',
        Location.joinWithSlash(`${this.basePath}`, `get-provider/${encodeURIComponent(id)}`),
        {
          observe: 'response',
          headers: this.userGetHeader.getHeader(),
        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  getProvderFromListByNpi(npi: any) {
    return this.http
      .request(
        'get',
        Location.joinWithSlash(`${this.basePath}`, `get-provider-npi/${encodeURIComponent(npi)}`),
        {
          observe: 'response',
          headers: this.userGetHeader.getHeader(),
        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  updateProvider(data: any, id) {
    console.log(data);
    return this.http
      .request(
        'put',
        Location.joinWithSlash(`${this.basePath}`, `update-provider/${id}`),
        {
          observe: 'response',
          headers: this.userGetHeader.getHeader(),
          body: JSON.stringify(data),
        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  filterProviderList(data: any) {
    return this.http
      .request(
        'get',
        Location.joinWithSlash(
          `${this.basePath}`,
          `filter-provider/${encodeURIComponent(data)}`
        ),
        {
          observe: 'response',
          headers: this.userGetHeader.getHeader(),
        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  public getHealthSystemName(orgname: string, npivalue: string) {
    return this.http
      .request(
        'get',
        Location.joinWithSlash(
          `${this.basePath}`,
          `healthsystem/${encodeURIComponent(npivalue)}/${encodeURIComponent(orgname)}`
        ),
        {
          observe: 'response',
          headers: this.headers,
        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  public getHealthSystemFirstName(firstname: string, npivalue: string) {
    return this.http
      .request(
        'get',
        Location.joinWithSlash(
          `${this.basePath}`,
          `healthsystemIndividualFirstName/${encodeURIComponent(npivalue)}/${encodeURIComponent(firstname)}`
        ),
        {
          observe: 'response',
          headers: this.headers,
        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  public getHealthSystemLastName(lastname: string, npivalue: string) {
    return this.http
      .request(
        'get',
        Location.joinWithSlash(
          `${this.basePath}`,
          `healthsystemIndividualLastName/${encodeURIComponent(npivalue)}/${encodeURIComponent(lastname)}`
        ),
        {
          observe: 'response',
          headers: this.headers,
        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  public getHealthSystemBothName(firstname: string, lastname: string, npivalue: string) {
    return this.http
      .request(
        'get',
        Location.joinWithSlash(
          `${this.basePath}`,
          `healthsystemIndividualBothName/${encodeURIComponent(firstname)}/${encodeURIComponent(lastname)}/${encodeURIComponent(npivalue)}`
        ),
        {
          observe: 'response',
          headers: this.headers,
        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  public getHealthSystemNameDefault() {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `healthsystem/usa/NPI-2`), {
        observe: 'response',
        headers: this.headers,
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  getUsaStateList() {
    var url = '../../assets/json/usa-statelist.json';
    return this.http.get(url);
  }

  adminDashboardList() {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `admin-user-list`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  adminDashboardTotalCount() {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `admin-user-count`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  adminUserGetInfoByEmail(data: any) {
    return this.http
      .request(
        'get',
        Location.joinWithSlash(
          `${this.basePath}`,
          `admin-user-get-by-email/${encodeURIComponent(data)}`
        ),
        {
          observe: 'response',
          headers: this.userGetHeader.getHeader(),
        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  userEnrollmentUpdate() {
    return this.http
      .request(
        'put',
        Location.joinWithSlash(
          `${this.basePath}`,
          `enrollmen-update`
        ),
        {
          observe: 'response',
          headers: this.userGetHeader.getHeader(),
        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }





  checkAccessTokenExists(oneUpUserId) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `patient-access-token`), {
        observe: 'response',
        body: JSON.stringify({ oneUpUserId }),
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  getAllSystemHealthList(token, name) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `system-health-list/${encodeURIComponent(token)}/${encodeURIComponent(name)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  //patient module work start
  uploadFilesPatient(formData) {
    return this.http.request('post', Location.joinWithSlash(`${this.basePath}`, `patient-upload-files`), {
      headers: this.userGetHeader.getHeaderWithMultipart(),
      observe: 'response',
      body: (formData)
    }).map((response: any) => {
      switch (response.status) {
        case 200: {
          return response.body;
        }
      }
    });
  }




  patientProviderAdd(data: any) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `patient-provider`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
        body: JSON.stringify(data),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }


  getPatientProviderList() {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `patient-provider`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  getPatientProviderListByUser(user_id){
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `patient-provider/${encodeURIComponent(user_id)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }



  getPatientProviderUploadList(upid) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `patient-provider-document/${encodeURIComponent(upid)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }






  getPatientProviderwithFileList() {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `patient-provider-with-files`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }






  deletePatientProviderFile(id: any) {
    return this.http.request('delete', Location.joinWithSlash(`${this.basePath}`, `patient-provider-document/${encodeURIComponent(id)}`), {
      observe: 'response',
      headers: this.userGetHeader.getHeader(),

    })
      .pipe(map(response => {
        switch (response.status) {
          case 200: {
            return response.body;
          }
        }
      }));
  }



  archivePatientProviderFile(id: any) {
    return this.http.request('delete', Location.joinWithSlash(`${this.basePath}`, `patient-provider-document-archive/${encodeURIComponent(id)}`), {
      observe: 'response',
      headers: this.userGetHeader.getHeader(),

    })
      .pipe(map(response => {
        switch (response.status) {
          case 200: {
            return response.body;
          }
        }
      }));
  }


  public getProvidertHealthSystem(npi: number) {
    return this.http
      .request(
        'get',
        Location.joinWithSlash(
          `${this.basePath}`,
          `singlehealthsystem/${encodeURIComponent(npi)}`
        ),
        {
          observe: 'response',
          headers: this.userGetHeader.getHeader(),
        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }


  //patient module work end

  //fitbit  module start here
  fitbitConfig(data: any) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `fitbit-config`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
        body: JSON.stringify(data),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }



  checkFitbitConfig() {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `check-fitbit-config`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  checkFitbitConfigByUser(user_id) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `check-fitbit-config-by-user/${encodeURIComponent(user_id)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }


  updateUserFitbitCode(data: any) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `user-fitbit-code`), {
        observe: 'response',
        headers:this.userGetHeader.getHeader(),
        body: JSON.stringify(data),
      })
      .map((response) => {
        switch (response.status) {
          case 200: {
            return response.body;
          }
        }
      });
  }

  getTodayActivities(access_token: any, tabFlag: any) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `today-activity/${encodeURIComponent(access_token)}/${encodeURIComponent(tabFlag)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }



  getlastTwoWeekActivity(access_token: any, tabFlag: any) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `last-two-week-activity/${encodeURIComponent(access_token)}/${encodeURIComponent(tabFlag)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  getlastTwoWeekCalories(access_token: any, tabFlag: any) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `last-two-week-calories/${encodeURIComponent(access_token)}/${encodeURIComponent(tabFlag)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }



  getlastTwoWeekInCalories(access_token: any, tabFlag: any) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `last-two-week-in-calories/${encodeURIComponent(access_token)}/${encodeURIComponent(tabFlag)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }


  getlastTwoWeekWeight(access_token: any, tabFlag: any) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `last-two-week-weight/${encodeURIComponent(access_token)}/${encodeURIComponent(tabFlag)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  getlastTwoWeekSleep(access_token: any, tabFlag: any) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `last-two-week-sleep/${encodeURIComponent(access_token)}/${encodeURIComponent(tabFlag)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }





  getlastWeekInOutCalories(access_token: any, tabFlag: any) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `last-week-in-out-calories/${encodeURIComponent(access_token)}/${encodeURIComponent(tabFlag)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }
  //fitbit module end here





  deletePatientProvider(id: any) {
    return this.http.request('delete', Location.joinWithSlash(`${this.basePath}`, `patient-provider/${encodeURIComponent(id)}`), {
      observe: 'response',
      headers: this.userGetHeader.getHeader(),

    })
      .pipe(map(response => {
        switch (response.status) {
          case 200: {
            return response.body;
          }
        }
      }));
  }



  // Google Health Data

  getAllGCPCloudHealthData() {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `gcp-health-data`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  // get patient Oneup User Id
  getpatientOneupUserIdSave(systemHealthId) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `all-oneup-user-id/${encodeURIComponent(systemHealthId)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  //get OneUp resource data
  getOneUpHealthResource(resourceType) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `oneUp-health-resource/${encodeURIComponent(resourceType)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }
  // get GCP resource data
  getGCPHealthResource(resourceType) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `gcp-health-resource/${encodeURIComponent(resourceType)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }
  // get GCP & Manual health resorce data
  getHealthData(user_type, user_id, resource_type){
    return this.http
    .request('get', Location.joinWithSlash(`${this.basePath}`, `health-data/${encodeURIComponent(user_type)}/${encodeURIComponent(user_id)}/${encodeURIComponent(resource_type)}`), {
      observe: 'response',
      headers: this.userGetHeader.getHeader(),
    })
    .pipe(
      map((response) => {
        switch (response.status) {
          case 200: {
            return response.body;
          }
        }
      })
    );
  }

  getManualGCPHealthResource(resourceType,type) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `gcp-health-resource/${encodeURIComponent(resourceType)}/${encodeURIComponent(type)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }
  
  // get all patient for provider by resource
  getAllPatient(type){
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `all-patient/${encodeURIComponent(type)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }
  /**
   * Create New Resource For Manual Medical Reord
   */

  createNewResource(data) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `new-resource`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
        body: JSON.stringify(data),

      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }
  
  getSharingResourceList() {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `sharing-resource`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }


  patientResourceSharingAdd(data: any) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `patient-resource-sharing`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
        body: JSON.stringify(data),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  getpatientResourceSharing() {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `patient-resource-sharing`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  deletepatientResourceSharing(patProvId: any) {
    return this.http.request('delete', Location.joinWithSlash(`${this.basePath}`, `patient-resource-sharing/${encodeURIComponent(patProvId)}`), {
      observe: 'response',
      headers: this.userGetHeader.getHeader(),

    })
      .pipe(map(response => {
        switch (response.status) {
          case 200: {
            return response.body;
          }
        }
      }));
  }


  getRole() {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `role`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  
  getApplicationPermission(roleId) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `appication-permission/${encodeURIComponent(roleId)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }
 

  applicationPermissionAdd(data: any,roleId) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `appication-permission`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
        body: JSON.stringify({data:data,roleId:roleId}),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  
  checkDemofitbitEnable() {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `fitbit-demo`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }


  checkmedicalRecordsAccess() {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `medical-record-aceess`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }
  


   
  getUserProfileList(userTypeId) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `user-profile-list/${encodeURIComponent(userTypeId)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  uploadProfileImage(formData) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `upload-profile-image`), {
        headers: this.userGetHeader.getHeaderWithMultipart(),
        observe: 'response',
        body: formData,
      })
      .map((response: any) => {
        switch (response.status) {
          case 200: {
            return response.body;
          }
        }
      });
  }

  updateUserProfile(data: any) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `update-user-profile`), {
        observe: 'response',
        headers: this.userGetHeader.getHeaderWithMultipart(),
        body: data,
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  
  updateProviderProfile(data: any) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `update-provider-profile`), {
        observe: 'response',
        headers: this.userGetHeader.getHeaderWithMultipart(),
        body: data,
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

  downloadDocument(id: any) {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'bearer ' + this.cookieService.get('token'));
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `document-download/${encodeURIComponent(id)}`), {
        observe: 'response',
        responseType: 'blob',
        headers,
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }



  updateDocumentFileName(data) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `document-filename-update`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
        body:data
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }


  getPatientListByType(typeId) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `provider-manage-access/${encodeURIComponent(typeId)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader()
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }


  providerManageAccessUser(data) {
    return this.http
      .request('post', Location.joinWithSlash(`${this.basePath}`, `provider-manage-access`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
        body:data
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }


  getPatientListByTypeWithFilter(searchFilter,userTypeId,searchDataValue) {
    return this.http
      .request('get', Location.joinWithSlash(`${this.basePath}`, `provider-manage-access/${encodeURIComponent(searchFilter)}/${encodeURIComponent(userTypeId)}/${encodeURIComponent(searchDataValue)}`), {
        observe: 'response',
        headers: this.userGetHeader.getHeader(),
      })
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }




    // get Contitions GCP resource data
    getContitionsGCPHealthcareResource(resourceType) {
      return this.http
        .request('get', Location.joinWithSlash(`${this.basePath}`, `gcp-health-resource/${encodeURIComponent(resourceType)}`), {
          observe: 'response',
          headers: this.userGetHeader.getHeader(),
        })
        .pipe(
          map((response) => {
            switch (response.status) {
              case 200: {
                return response.body;
              }
            }
          })
        );
    }




       // get Allergies GCP resource data
       getAllergiesGCPHealthcareResource(resourceType) {
        return this.http
          .request('get', Location.joinWithSlash(`${this.basePath}`, `gcp-health-resource/${encodeURIComponent(resourceType)}`), {
            observe: 'response',
            headers: this.userGetHeader.getHeader(),
          })
          .pipe(
            map((response) => {
              switch (response.status) {
                case 200: {
                  return response.body;
                }
              }
            })
          );
      }



       // get Immunizations GCP resource data
       getImmunizationsGCPHealthcareResource(resourceType) {
        return this.http
          .request('get', Location.joinWithSlash(`${this.basePath}`, `gcp-health-resource/${encodeURIComponent(resourceType)}`), {
            observe: 'response',
            headers: this.userGetHeader.getHeader(),
          })
          .pipe(
            map((response) => {
              switch (response.status) {
                case 200: {
                  return response.body;
                }
              }
            })
          );
      }


         // get Observations GCP resource data
         getObservationsGCPHealthcareResource(resourceType) {
          return this.http
            .request('get', Location.joinWithSlash(`${this.basePath}`, `gcp-health-resource/${encodeURIComponent(resourceType)}`), {
              observe: 'response',
              headers: this.userGetHeader.getHeader(),
            })
            .pipe(
              map((response) => {
                switch (response.status) {
                  case 200: {
                    return response.body;
                  }
                }
              })
            );
        }




    // Get GCP HealthcareResource data By ResourceType
    getGCPHealthcareResourceByResourceType(resourceType) {
      return this.http
        .request('get', Location.joinWithSlash(`${this.basePath}`, `gcp-health-resource/${encodeURIComponent(resourceType)}`), {
          observe: 'response',
          headers: this.userGetHeader.getHeader(),
        })
        .pipe(
          map((response) => {
            switch (response.status) {
              case 200: {
                return response.body;
              }
            }
          })
        );
    }
        

         //  update Gcp Heath Data
         updateGcpHeathData(resourceData,resourceType) {
          return this.http
            .request('put', Location.joinWithSlash(`${this.basePath}`, `gcp-health-resource/${encodeURIComponent(resourceType)}`), {
              observe: 'response',
              headers: this.userGetHeader.getHeader(),
              body:resourceData
            })
            .pipe(
              map((response) => {
                switch (response.status) {
                  case 200: {
                    return response.body;
                  }
                }
              })
            );
        }

        // Check Patient Exist or not from OneUp or Manual entry
        checkAnyPatientIfExist(type){
          return this.http
          .request('get', Location.joinWithSlash(`${this.basePath}`, `local-db-patient-check/${encodeURIComponent(type)}`), {
            observe: 'response',
            headers: this.userGetHeader.getHeader(),
          })
          .pipe(
            map((response) => {
              switch (response.status) {
                case 200: {
                  return response.body;
                }
              }
            })
          );
        }
        
    

      
  
  
  
  
  
}
