import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class UserAuthenticationCheckService {
  cookieValue: any;
  constructor(private cookieService: CookieService) { }

  getHeader() {
    let headers = new HttpHeaders();
    this.cookieValue = this.cookieService.get('token');
    headers = headers.set('Accept', 'application/json');
    headers = headers.set('Content-Type', 'application/json');
    //headers = headers.set('Content-Type', 'multipart/form-data');
    headers = headers.set('Authorization', 'bearer ' + this.cookieValue);

    return headers;
  }


  getHeaderWithMultipart() {
    let headers = new HttpHeaders();
    this.cookieValue = this.cookieService.get('token');
    //headers = headers.set('mimeType', "multipart/form-data");
    headers = headers.set('Accept', "multipart/form-data");
    headers = headers.set('Accept', 'application/json');
    headers = headers.set('Authorization', 'bearer ' + this.cookieValue);

    return headers;
  }


}
