import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpParams} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { Location } from '@angular/common';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/throw';
import { filter, map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  basePath: string;
  getHeaderComman() {
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/json');
    headers = headers.set('Content-Type', 'application/json');
    return headers;
  }



  constructor(private http:HttpClient,private cookieService: CookieService) {

    this.basePath = environment.apiBaseUrl;

   }
  login(authCredentials) {
    return this.http.post(environment.apiBaseUrl + 'userAuthenticate', authCredentials);
  }

  
   isLoggedInUser() {
    return this.cookieService.get('token')
  }

  deleteToken() {
    return this.cookieService.delete('token');
  }

  Logout() {
    return this.http.get(environment.apiBaseUrl + 'userLogout');
  }


  

  checkEmail(data:any) {
    let headers = this.getHeaderComman();
    return this.http.request('post', Location.joinWithSlash(`${this.basePath}`, `checkEmail`), {
      observe: 'response',
      headers:headers,
      body: JSON.stringify({'email':data})
    })
    .pipe(map(response =>{
      switch (response.status){
        case 200:{
          return response.body;
        }
      }
    }))
  }

  
sendRestLink(data: any) {
  let headers = this.getHeaderComman();
  return this.http.request('post', Location.joinWithSlash(`${this.basePath}`, `emailVerifyPass`), {
    observe: 'response',
    headers : headers,
    body: JSON.stringify(data)

  })
  .pipe(map(response => {
      switch (response.status) {
        case 200: {
          return response.body;
        }
      }
    }))
}


userChangePassword(data: any,token:any) {

  let headers = this.getHeaderComman();
    return this.http.request('put', Location.joinWithSlash(`${this.basePath}`, `userChangePassword/${encodeURIComponent(token)}`), {
    observe: 'response',
    headers : headers,
    body: JSON.stringify(data)

  })
    .pipe(map(response => {
      switch (response.status) {
        case 200: {
          return response.body;
        }
      }
    }))
}


    
}
