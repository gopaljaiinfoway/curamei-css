import { Component, OnInit, Inject,Output, EventEmitter  } from '@angular/core';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material/dialog';
import { calories } from './calories';
   
@Component({
  selector: 'app-fitbit-calories-dialog',
  templateUrl: './fitbit-calories-dialog.component.html',
  styleUrls: ['./fitbit-calories-dialog.component.css']
})
export class FitbitCaloriesDialogComponent implements OnInit {
  calories: any[]
  view: any[]

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';


  lastTwoWeekAVgCaloriesOut = 0;
  lastTwoWeekTotalCaloriesOut = 0;


  colorSchemeCalories = {
    domain: ['#FC6400', '#FAC000']
  };

  constructor (public dialogRef: MatDialogRef<FitbitCaloriesDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    //Object.assign(this, { calories })
  }

  ngOnInit(): void {
    Object.assign(this, { calories : this.data.weeksGraph })
    this.lastTwoWeekAVgCaloriesOut = this.data.lastTwoWeekAVgCaloriesOut;
    this.lastTwoWeekTotalCaloriesOut = this.data.lastTwoWeekTotalCaloriesOut;
  }

  close() {
    this.dialogRef.close();
  }

}
