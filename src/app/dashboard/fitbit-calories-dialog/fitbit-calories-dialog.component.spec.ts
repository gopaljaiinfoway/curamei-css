import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitbitCaloriesDialogComponent } from './fitbit-calories-dialog.component';

describe('FitbitCaloriesDialogComponent', () => {
  let component: FitbitCaloriesDialogComponent;
  let fixture: ComponentFixture<FitbitCaloriesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitbitCaloriesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitbitCaloriesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
