import { Component, OnInit, Inject,Output, EventEmitter  } from '@angular/core';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material/dialog';
import { activity } from './activity';
@Component({
  selector: 'app-fitbit-activity-dialog',
  templateUrl: './fitbit-activity-dialog.component.html',
  styleUrls: ['./fitbit-activity-dialog.component.css']
})
export class FitbitActivityDialogComponent implements OnInit {
  activity: any[]
  view: any[]

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';
  lastTwoWeekTotalSteps=0;
  lastTwoWeekAVgSteps=0;
  colorScheme = {
    domain: ['#7209B7', '#3A0CA3', '#4361EE']
  };
  constructor (public dialogRef: MatDialogRef<FitbitActivityDialogComponent>,@Inject(MAT_DIALOG_DATA) public data: any) {
    //Object.assign(this, { activity })
  }

  ngOnInit(): void {
    Object.assign(this, { activity : this.data.weeksGraph })
    this.lastTwoWeekTotalSteps = this.data.lastTwoWeekTotalSteps;
    this.lastTwoWeekAVgSteps = this.data.lastTwoWeekAVgSteps;
  }

  close() {
    this.dialogRef.close();
  }



}
