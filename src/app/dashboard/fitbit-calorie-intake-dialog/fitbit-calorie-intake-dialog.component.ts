import { Component, OnInit, Inject,Output, EventEmitter  } from '@angular/core';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material/dialog';
import { calorieintake } from './calorie-intake';
import { weekcalorie } from './week-calorie';
@Component({
  selector: 'app-fitbit-calorie-intake-dialog',
  templateUrl: './fitbit-calorie-intake-dialog.component.html',
  styleUrls: ['./fitbit-calorie-intake-dialog.component.css']
})
export class FitbitCalorieIntakeDialogComponent implements OnInit {
  calorieintake: any[]
  weekcalorie: any[]
  view: any[]
  viewweek: any[] = [600, 300]

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';
  legendPosition: string = 'right';
  legend: boolean = true;
  lastTwoWeekTotalCaloriesIn =0;

  colorSchemeTakeIn = {
    domain: ['#D19477', '#E5B5A1']
  };

  colorSchemeIn = {
    domain: ['#c58c85', '#ecbcb4', '#d1a3a4', '#a1665e', '#503335', '#592f2a', '#D19477']
  }
  constructor (public dialogRef: MatDialogRef<FitbitCalorieIntakeDialogComponent>,@Inject(MAT_DIALOG_DATA) public data: any) {
   // Object.assign(this, { calorieintake })
   // Object.assign(this, { weekcalorie })
  }
 
  ngOnInit(): void {
    this.lastTwoWeekTotalCaloriesIn = this.data.lastTwoWeekTotalCaloriesIn;
     Object.assign(this, { calorieintake : this.data.weeksGraph });
    Object.assign(this, { weekcalorie : this.data.oneWeekGraph })
     
  }

  close() {
    this.dialogRef.close();
  }

}
