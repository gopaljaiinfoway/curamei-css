export var weekcalorie = [
  {
    "name": "Monday",
    "value": 2456
  },
  {
    "name": "Tuesday",
    "value": 1456
  },
  {
    "name": "Wednesday",
    "value": 456
  },
  {
    "name": "Thursday",
    "value": 2006
  },
  {
    "name": "Friday",
    "value": 3000
  },
  {
    "name": "Saturday",
    "value": 500
  },
  {
    "name": "Sunday",
    "value": 456
  },
];
