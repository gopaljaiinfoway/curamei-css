import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitbitCalorieIntakeDialogComponent } from './fitbit-calorie-intake-dialog.component';

describe('FitbitCalorieIntakeDialogComponent', () => {
  let component: FitbitCalorieIntakeDialogComponent;
  let fixture: ComponentFixture<FitbitCalorieIntakeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitbitCalorieIntakeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitbitCalorieIntakeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
