import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommmanService } from '../../_services/commman.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-fitbit-settings',
  templateUrl: './fitbit-settings.component.html',
  styleUrls: ['./fitbit-settings.component.css']
})
export class FitbitSettingsComponent implements OnInit {
  fitbitConfigForm: FormGroup;
  buttonText='Submit';
  currentURL:any;
  constructor (private router: Router,public dialogRef: MatDialogRef<FitbitSettingsComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
  private commanService: CommmanService) {
  
   }
  ngOnInit(): void {
    this.fitbitConfigForm = new FormGroup({
      clientid: new FormControl('', [Validators.required]),
      clientsecret: new FormControl('', [Validators.required]),
      callbackurl: new FormControl('', [Validators.required])
    })
  }

  close() {
    this.dialogRef.close();
  }
  onSubmit() {
   this.buttonText = 'Sending...'
    if(this.fitbitConfigForm.valid){
      this.commanService.fitbitConfig(this.fitbitConfigForm.value).subscribe(
        (resmsg: any) => {
            if(resmsg.success==true){
              window.location.href = resmsg.url;
             }
        this.buttonText="Submit";
        this.dialogRef.close();
        },
        (err) => {
             console.log(err);
        }
      )
    }
    }
}
