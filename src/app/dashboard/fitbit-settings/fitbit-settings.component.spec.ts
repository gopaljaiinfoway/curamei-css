import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitbitSettingsComponent } from './fitbit-settings.component';

describe('FitbitSettingsComponent', () => {
  let component: FitbitSettingsComponent;
  let fixture: ComponentFixture<FitbitSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitbitSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitbitSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
