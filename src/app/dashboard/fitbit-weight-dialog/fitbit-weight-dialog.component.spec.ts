import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitbitWeightDialogComponent } from './fitbit-weight-dialog.component';

describe('FitbitWeightDialogComponent', () => {
  let component: FitbitWeightDialogComponent;
  let fixture: ComponentFixture<FitbitWeightDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitbitWeightDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitbitWeightDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
