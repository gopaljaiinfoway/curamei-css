export var sleep = [
  {
    "name": "01",
    "value": 10000
  },
  {
    "name": "02",
    "value": 7000
  },
  {
    "name": "03",
    "value": 8000
  },
  {
    "name": "04",
    "value": 5000
  },
  {
    "name": "05",
    "value": 9500
  },
  {
    "name": "06",
    "value": 8000
  },
  {
    "name": "07",
    "value": 8200
  },
  {
    "name": "08",
    "value": 2500
  },
  {
    "name": "09",
    "value": 6000
  },
  {
    "name": "10",
    "value": 2500
  },
  {
    "name": "11",
    "value": 8000
  },
  {
    "name": "12",
    "value": 7500
  },
  {
    "name": "13",
    "value": 7000
  },
  {
    "name": "14",
    "value": 1000
  },

];