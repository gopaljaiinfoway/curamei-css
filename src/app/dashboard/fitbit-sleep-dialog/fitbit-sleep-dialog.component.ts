import { Component, OnInit, Inject,Output, EventEmitter  } from '@angular/core';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material/dialog';
import { sleep } from './sleep';
 
@Component({
  selector: 'app-fitbit-sleep-dialog',
  templateUrl: './fitbit-sleep-dialog.component.html',
  styleUrls: ['./fitbit-sleep-dialog.component.css']
})
export class FitbitSleepDialogComponent implements OnInit {
  sleep: any[]
  view: any[]

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';

  weeksAvgSleepHours=0;
  weeksHours=0;
  weeksMinutes=0;

  colorSchemeSleep = {
    domain: ['#0D1821', '#344966']
  }

  constructor (public dialogRef: MatDialogRef<FitbitSleepDialogComponent>,@Inject(MAT_DIALOG_DATA) public data: any) {
    
  }

  ngOnInit(): void {
    Object.assign(this, { sleep:this.data.sleepWeeksGraph })
    this.weeksAvgSleepHours = this.data.weeksAvgSleepHours;
    this.weeksHours = this.data.weeksHours;
  }

  close() {
    this.dialogRef.close();
  }



}
