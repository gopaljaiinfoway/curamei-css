import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { CommmanService } from '../../_services/commman.service';
import { FitbitActivityDialogComponent } from '../fitbit-activity-dialog/fitbit-activity-dialog.component';
import { FitbitCalorieIntakeDialogComponent } from '../fitbit-calorie-intake-dialog/fitbit-calorie-intake-dialog.component';
import { FitbitCalorieoutDialogComponent } from '../fitbit-calorieout-dialog/fitbit-calorieout-dialog.component';
import { FitbitCaloriesDialogComponent } from '../fitbit-calories-dialog/fitbit-calories-dialog.component';
import { FitbitSettingsComponent } from '../fitbit-settings/fitbit-settings.component';
import { FitbitSleepDialogComponent } from '../fitbit-sleep-dialog/fitbit-sleep-dialog.component';
import { FitbitWeightDialogComponent } from '../fitbit-weight-dialog/fitbit-weight-dialog.component';
import { single } from './today-activity';
@Component({
  selector: 'app-fitbit-dashboard',
  templateUrl: './fitbit-dashboard.component.html',
  styleUrls: ['./fitbit-dashboard.component.css']
})
export class FitbitDashboardComponent implements OnInit {

  
  @Input() user_id: any = null;

  single: any[];
  steps: any[];
  weight: any[];
  calories: any[];
  view: any[];
  fitbitPopup: boolean = true;
  params: any;
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';
  tabFlag: boolean = false;
  liveFlagTab: boolean = false;
  demoFlagTab: boolean = true;
  //activity keys
  lastTwoWeekTotalSteps = 0;
  lastTwoWeekAVgSteps = 0;
  activityPopupModel: any;
  todayStep = 0;
  lastTwoWeekTotalStepsFlag: boolean = false;
  todayStepFlag: boolean = false;

  //Caloriesout keys
  todayCaloriesOut = 0;
  lastTwoWeekAVgCaloriesOut = 0;
  lastTwoWeekTotalCaloriesOut = 0;
  caloriesPopupModel: any;
  lastTwoWeekTotalCaloriesOutFlag: boolean = false;
  todayCaloriesOutFlag: boolean = false;

  //CaloriesIn keys
  lastTwoWeekTotalCaloriesIn = 0;
  caloriesInPopupModel: any;
  todayCaloriesIn = 0;
  lastTwoWeekTotalCaloriesInFlag: boolean = false;
  todayCaloriesInFlag: boolean = false;
  caloriesInOut;


  //weight keys
  todayWeight = 0;
  weightPopupModel: any;
  todayWeightFlag: boolean = false;
  todayWeightGraph = 0;
  //seep keys
  sleepWeeksTotal = 0;
  todayHours = 0;
  todayMinutes = 0;
  weeksHours = 0;
  weeksMinutes = 0;
  sleepPopupModel: any;
  weeksHoursFlag: boolean = false;
  todayHoursFlag: boolean = false;

  //Carories in/out key
  caloriesInOutPopupModel: any;
  lastWeekTotalDiff = 0;
  weekCaloriesInOutFlag: boolean = false;
  colorScheme = {
    domain: ['#7209B7', '#3A0CA3', '#4361EE']
  };

  colorSchemeCalories = {
    domain: ['#FC6400', '#FAC000']
  };

  colorSchemeWeight = {
    domain: ['#184d47', '#96bb7c']
  };

  colorSchemeTakeIn = {
    domain: ['#D19477', '#E5B5A1']
  };

  colorSchemeSleep = {
    domain: ['#0D1821', '#344966']
  };

  colorSchemeSCalorieOut = {
    domain: ['#810000', '#ff6464']
  };

  //Demo fitbit flag
flagDemoFitbitShow: boolean = false;
  providerFlag: Boolean = false;
  byProviderTextFlag: Boolean = false;

  constructor (public dialog: MatDialog, private route: ActivatedRoute, private router: Router, private commanService: CommmanService,
    private spinner: NgxSpinnerService) {
    Object.assign(this, { single });
    // Object.assign(this, { steps })
    // Object.assign(this, { calories })
    // Object.assign(this, { weight })
    this.params = this.route.snapshot.queryParams["code"];
    console.log("this.params");
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);
  }

  ngOnInit(): void {
    if(this.router.url == '/patient/fitbit-dashboard'){
      this.providerFlag = true;
      this.DemofitbitEnable();
    }else{
      this.liveData()
      this.byProviderTextFlag = true;
      this.providerFlag = false;
    }
    this.updateCode();
  }

  onSelect(event) {
    console.log(event);
  }

  demoData() {
    this.liveFlagTab = false;
    this.demoFlagTab = true;
    this.tabFlag = false;

    this.lastTwoWeekTotalStepsFlag = false;
    this.todayStepFlag = false;
    this.lastTwoWeekTotalCaloriesOutFlag = false;
    this.todayCaloriesOutFlag = false;
    this.lastTwoWeekTotalCaloriesInFlag = false;
    this.todayCaloriesInFlag = false;
    this.todayWeightFlag = false;
    this.weeksHoursFlag = false;
    this.todayHoursFlag = false;
    this.weekCaloriesInOutFlag = false;

    this.getTodayActivities(null, this.tabFlag);
    this.getlastTwoWeekActivity(null, this.tabFlag);
    this.getlastTwoWeekCalories(null, this.tabFlag);
    this.getlastTwoWeekInCalories(null, this.tabFlag);
    this.getlastTwoWeekWeight(null, this.tabFlag);
    this.getlastTwoWeekSleep(null, this.tabFlag);
    this.getlastWeekInOutCalories(null, this.tabFlag);


  }

  liveData() {
    this.liveFlagTab = true;
    this.demoFlagTab = false;
    this.tabFlag = true;

    this.lastTwoWeekTotalStepsFlag = false;
    this.todayStepFlag = false;
    this.lastTwoWeekTotalCaloriesOutFlag = false;
    this.todayCaloriesOutFlag = false;
    this.lastTwoWeekTotalCaloriesInFlag = false;
    this.todayCaloriesInFlag = false;
    this.todayWeightFlag = false;
    this.weeksHoursFlag = false;
    this.todayHoursFlag = false;
    if(this.providerFlag){
      this.checkUserConfigured();
    }else{

    }
  }

  openFitBitSleepDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "40%";
    dialogConfig.panelClass = "mob-fitbit-dialog";
    dialogConfig.data = this.sleepPopupModel;
    const dialogRef = this.dialog.open(FitbitSleepDialogComponent, dialogConfig);
  }


  openWeightDialog() {
    const dialogConfig = new MatDialogConfig();
    // dialogConfig.data = this.particularProviderRecord;
    dialogConfig.width = "40%";
    dialogConfig.panelClass = "mob-fitbit-dialog";
    dialogConfig.data = this.weightPopupModel;
    const dialogRef = this.dialog.open(FitbitWeightDialogComponent, dialogConfig);
  }


  openActivityDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "40%";
    dialogConfig.panelClass = "mob-fitbit-dialog";
    dialogConfig.data = this.activityPopupModel;
    const dialogRef = this.dialog.open(FitbitActivityDialogComponent, dialogConfig);
  }

  openCaloriesDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "40%";
    dialogConfig.panelClass = "mob-fitbit-dialog";
    dialogConfig.data = this.caloriesPopupModel;
    const dialogRef = this.dialog.open(FitbitCaloriesDialogComponent, dialogConfig);
  }

  openCaloriesIntakeDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "40%";
    dialogConfig.panelClass = "mob-fitbit-dialog";
    dialogConfig.data = this.caloriesInPopupModel;
    const dialogRef = this.dialog.open(FitbitCalorieIntakeDialogComponent, dialogConfig);
  }

  openFitBitSettingDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "40%";
    dialogConfig.panelClass = "mob-fitbit-dialog";
    const dialogRef = this.dialog.open(FitbitSettingsComponent, dialogConfig);
  }
  viewFitbitByUser(userId){
    this.commanService.checkFitbitConfigByUser(userId)
    .subscribe(
      (resmsg: any) => {
        console.log(resmsg)
        if(resmsg.success){
          this.byProviderTextFlag = false;
          localStorage.setItem('fitbit_refresh_token', resmsg.data);
          this.getTodayActivities(resmsg.data, this.tabFlag);
          this.getlastTwoWeekActivity(resmsg.data, this.tabFlag);
          this.getlastTwoWeekCalories(resmsg.data, this.tabFlag);
          this.getlastTwoWeekInCalories(resmsg.data, this.tabFlag);
          this.getlastTwoWeekWeight(resmsg.data, this.tabFlag);
          this.getlastTwoWeekSleep(resmsg.data, this.tabFlag);
        }
      }, (err) => {
        console.log(err)
      })
  }
  checkUserConfigured() {
    this.commanService.checkFitbitConfig().subscribe(
      (resmsg: any) => {
        console.log('+++++++++++++++++++++++++++++++++++++++++++++++++++')
        console.log(resmsg)
        if (resmsg.success == true) {
          this.fitbitPopup = false;
          localStorage.setItem('fitbit_refresh_token', resmsg.data);
          this.getTodayActivities(resmsg.data, this.tabFlag);
          this.getlastTwoWeekActivity(resmsg.data, this.tabFlag);
          this.getlastTwoWeekCalories(resmsg.data, this.tabFlag);
          this.getlastTwoWeekInCalories(resmsg.data, this.tabFlag);
          this.getlastTwoWeekWeight(resmsg.data, this.tabFlag);
          this.getlastTwoWeekSleep(resmsg.data, this.tabFlag);
        }else if(resmsg.success=='redirect'){
             if(confirm('You will be redirected to Fitbit Authorization Page. Do you want to proceed?'))
              window.location.href = resmsg.url;
              else
              this.router.navigateByUrl('/patient/dashboard')
        }
        else {
         // this.fitbitPopup = true;
         // this.openFitBitSettingDialog();
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }



  openFitBitCalorieOutDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "40%";
    dialogConfig.panelClass = "mob-fitbit-dialog";
    dialogConfig.data = this.caloriesInOutPopupModel;
    const dialogRef = this.dialog.open(FitbitCalorieoutDialogComponent, dialogConfig);
  }


  updateCode() {
    if (this.params) {
      this.commanService.updateUserFitbitCode({ 'code': this.params }).subscribe((result: any) => {
        if (result.success == true) {
          localStorage.setItem('fitbit_refresh_token', result.data);
          this.router.navigateByUrl('/patient/fitbit-dashboard');
        } else {
        }
      });
    }
  }



  getTodayActivities(access_token, tabFlag) {
    this.commanService.getTodayActivities(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {

          this.todayStepFlag = true;
          this.todayCaloriesOutFlag = true;
          this.todayStep = Math.round(resmsg.activityObj.steps);
          this.todayCaloriesOut = Math.round(resmsg.activityObj.caloriesOut);

        } else {

        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getlastTwoWeekActivity(access_token, tabFlag) {
    this.commanService.getlastTwoWeekActivity(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.lastTwoWeekTotalStepsFlag = true;
          console.log(resmsg.activityObj.oneGraphActivity);
          this.activityPopupModel = resmsg.activityObj;
          this.lastTwoWeekTotalSteps = Math.round(resmsg.activityObj.lastTwoWeekTotalSteps);
          this.lastTwoWeekAVgSteps = resmsg.activityObj.lastTwoWeekAVgSteps;
          Object.assign(this, { steps: resmsg.activityObj.oneGraphActivity });
        } else {

        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getlastTwoWeekCalories(access_token, tabFlag) {
    this.commanService.getlastTwoWeekCalories(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.lastTwoWeekTotalCaloriesOutFlag = true;
          Object.assign(this, { calories: resmsg.caloriesObj.oneGraphCalories });
          this.lastTwoWeekAVgCaloriesOut = resmsg.caloriesObj.lastTwoWeekAVgCaloriesOut;
          this.lastTwoWeekTotalCaloriesOut = resmsg.caloriesObj.lastTwoWeekTotalCaloriesOut;
          this.caloriesPopupModel = resmsg.caloriesObj;
        } else {

        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getlastTwoWeekInCalories(access_token, tabFlag) {
    this.commanService.getlastTwoWeekInCalories(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.lastTwoWeekTotalCaloriesInFlag = true;
          this.todayCaloriesInFlag = true;
          Object.assign(this, { weight: resmsg.caloriesInObj.oneGraphCaloriesIn });
          this.lastTwoWeekTotalCaloriesIn = Math.round(resmsg.caloriesInObj.lastTwoWeekTotalCaloriesIn);
          this.caloriesInPopupModel = resmsg.caloriesInObj;
          this.todayCaloriesIn = Math.round(resmsg.caloriesInObj.todayCaloriesIn);
        } else {

        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getlastTwoWeekWeight(access_token, tabFlag) {
    this.commanService.getlastTwoWeekWeight(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.todayWeightFlag = true;
          this.todayWeight = resmsg.weightObj.todayWeight;
          this.weightPopupModel = resmsg.weightObj;
          Object.assign(this, { todayWeightGraph: resmsg.weightObj.todayWeightGraph });

        } else {

        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getlastTwoWeekSleep(access_token, tabFlag) {
    this.commanService.getlastTwoWeekSleep(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {

          Object.assign(this, { sleepWeeksTotal: resmsg.sleepObj.oneGraphWeeksSleep });
          this.sleepPopupModel = resmsg.sleepObj;
          // this.sleepWeeksTotal=resmsg.sleepObj.sleepWeeksTotal;
          this.todayHours = resmsg.sleepObj.todayHours;
          this.todayMinutes = resmsg.sleepObj.todayMinutes;
          this.weeksHours = resmsg.sleepObj.weeksHours;
          this.weeksMinutes = resmsg.sleepObj.weeksMinutes;
          this.weeksHoursFlag = true;
          this.todayHoursFlag = true;

        } else {

        }
      },
      (err) => {
        console.log(err);
      }
    );
  }


  getlastWeekInOutCalories(access_token, tabFlag) {
    this.commanService.getlastWeekInOutCalories(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.caloriesInOutPopupModel = resmsg.caloriesInOutObj;
          this.lastWeekTotalDiff = resmsg.caloriesInOutObj.lastWeekTotalDiff;
          Object.assign(this, { caloriesInOut: resmsg.caloriesInOutObj.oneWeekGraph });

          this.weekCaloriesInOutFlag = true;
        } else {

        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  DemofitbitEnable() {
    this.commanService.checkDemofitbitEnable().subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.flagDemoFitbitShow =true;
          this.demoData();
        }else{
          this.flagDemoFitbitShow =false;
          this.liveData();
        } 
      },
      (err) => {
        console.log(err);
      }
    );
  }


}
