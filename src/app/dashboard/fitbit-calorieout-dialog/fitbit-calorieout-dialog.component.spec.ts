import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitbitCalorieoutDialogComponent } from './fitbit-calorieout-dialog.component';

describe('FitbitCalorieoutDialogComponent', () => {
  let component: FitbitCalorieoutDialogComponent;
  let fixture: ComponentFixture<FitbitCalorieoutDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitbitCalorieoutDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitbitCalorieoutDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
