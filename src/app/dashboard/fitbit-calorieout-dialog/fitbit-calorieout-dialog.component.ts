import { Component, OnInit, Inject,Output, EventEmitter  } from '@angular/core';
import { calorieout } from './calorieout';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-fitbit-calorieout-dialog',
  templateUrl: './fitbit-calorieout-dialog.component.html',
  styleUrls: ['./fitbit-calorieout-dialog.component.css']
})
export class FitbitCalorieoutDialogComponent implements OnInit {
  
  calorieout: any[]
  view: any[]

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';
  legendPosition = 'below'
  lastWeekTotalCaloriesIn =0;
  lastWeekTotalCaloriesOut = 0;
  lastWeekTotalDiff = 0;
  colorSchemeSCalorieOut = {
    domain: ['#810000', '#ff6464']
  };
  constructor (public dialogRef: MatDialogRef<FitbitCalorieoutDialogComponent>,@Inject(MAT_DIALOG_DATA) public data: any) {
   // Object.assign(this, { calorieout })
     console.log("----------------------------");
     console.log(this.data);
     console.log("----------------------------");
  }

  ngOnInit(): void {
    this.lastWeekTotalCaloriesIn = this.data.lastWeekTotalCaloriesIn;
    this.lastWeekTotalCaloriesOut = this.data.lastWeekTotalCaloriesOut;
    this.lastWeekTotalDiff = this.data.lastWeekTotalDiff;
   Object.assign(this, { calorieout : this.data.weeksGraph })
  }

  close() {
    this.dialogRef.close();
  }

}
