import { Injectable } from '@angular/core';
import { CanActivate,ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {CommmanService} from '../_services/commman.service';

@Injectable({
  providedIn: 'root'
})
export class ActivateGuard implements  CanActivate{
constructor(private userLog:CommmanService, private router:Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
       if (!this.userLog.isLoggedInUser()) {
        this.router.navigateByUrl('/signin');
        this.userLog.deleteToken();
        return false;
      }
    return true;
    }
  
}
