import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-practitioners',
  templateUrl: './practitioners.component.html',
  styleUrls: ['./practitioners.component.css']
})
export class PractitionersComponent implements OnInit {
  panelOpenState: boolean;
  practitionerRecord = [{
    key: 1,
    title: "primary",
    subtitle: "secondary",
    fieldRecords: [{
      key: 1,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }],
  }, {
    key: 2,
    title: "primary",
    subtitle: "secondary",
    fieldRecords: [{
      key: 1,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }],
  }, {
    key: 3,
    title: "primary",
    subtitle: "secondary",
    fieldRecords: [{
      key: 1,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }],
  }, {
    key: 4,
    title: "primary",
    subtitle: "secondary",
    fieldRecords: [{
      key: 1,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }],
  }, {
    key: 5,
    title: "primary",
    subtitle: "secondary",
    fieldRecords: [{
      key: 1,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }],
  }
  ]

  constructor () { }

  ngOnInit(): void {
  }

}
