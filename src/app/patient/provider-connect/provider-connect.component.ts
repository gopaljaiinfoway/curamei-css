import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommmanService } from 'src/app/_services/commman.service';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-provider-connect',
  templateUrl: './provider-connect.component.html',
  styleUrls: ['./provider-connect.component.css']
})
export class ProviderConnectComponent implements OnInit {
  organisationData: any
  records = [];
  returnedArray = []
  finalRecord: number
  p: number = 1
  panelOpenState = false
  accessTokenId: any;
  client_id: any;
  userobj: any;
  filterName: any;
  dataFlg: boolean = false;
  errorMessage: any;
  dataFoundMessage = 'Loading...';
  constructor (private commanService: CommmanService, private router: Router) {
    this.client_id = environment.client_id;
    this.userobj = JSON.parse(localStorage.getItem('user'));
    this.accessTokenId = this.userobj.access_token;
    this.filterName = 'mi';
  }

  ngOnInit(): void {
    this.SystemHealthList();
  }

  goToSelectDataSource() {
    this.router.navigateByUrl('/patient/request-health-system')
  }




  SystemHealthList(){
    this.records=[];
    this.dataFoundMessage='Loading...';
    this.commanService.getAllSystemHealthList(this.accessTokenId,this.filterName).subscribe(
      (res: any) => {  
                  if(res.success && res.data.length > 0){
                   this.records =  res.data;
                  }else{
                    this.records=[];
                     this.errorMessage = res.message;
                     this.dataFoundMessage='Data is not available.';
                  }
                   
      },
      (err) => {
        console.log("not get data");
      }
    )
  }

  redictOneUPWeb(clinicalId) {
    if (confirm("Are you sure you want to confirm")) {
      localStorage.setItem('providerSystemHealthId',clinicalId);
      window.location.href = `https://api.1up.health/connect/system/clinical/${clinicalId}?client_id=${this.client_id}&access_token=${this.accessTokenId}`;

    }
  }


  searchFilter(data) {
    if (data == '') {
      this.filterName = 'mi';
      this.records = [];
      this.SystemHealthList();
   }
     if(data.length > 1){
         this.filterName=data;
         console.log(this.filterName)
         this.records=[];
         this.SystemHealthList();
      }
     
}



}
