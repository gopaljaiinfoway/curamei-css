import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { DataSourcesComponent } from './data-sources/data-sources.component'
import { EditProviderDialogComponent } from './edit-provider-dialog/edit-provider-dialog.component'
import { Enrollment1Component } from './enrollment1/enrollment1.component'
import { Enrollment2Component } from './enrollment2/enrollment2.component'
import { Enrollment3Component } from './enrollment3/enrollment3.component'
import { Enrollment4Component } from './enrollment4/enrollment4.component'
import { MedicalRecordsComponent } from './medical-records/medical-records.component'
import { PatientProviderComponent } from './patient-provider/patient-provider.component'
import { PatientRoutingModule } from './patient-routing.module'
import { PdashboardComponent } from './pdashboard/pdashboard.component'
import { ProviderListComponent } from './provider-list/provider-list.component';
import { PractitionersComponent } from './practitioners/practitioners.component';
import { EncountersComponent } from './encounters/encounters.component';
import { RequestHealthSystemComponent } from './request-health-system/request-health-system.component';
import { SelectDataSourceComponent } from './select-data-source/select-data-source.component';
import { ManualMedicalRecordsComponent } from './manual-medical-records/manual-medical-records.component';
import { SharePatientResourceComponent } from './share-patient-resource/share-patient-resource.component';
import { MedicalRecordsDetailsComponent } from './medical-records/medical-records-details/medical-records-details.component'

@NgModule({
  declarations: [
    PdashboardComponent,
    Enrollment1Component,
    Enrollment2Component,
    Enrollment3Component,
    Enrollment4Component,
    DataSourcesComponent,
    PatientProviderComponent,
    ProviderListComponent,
    EditProviderDialogComponent,
    MedicalRecordsComponent,
    PractitionersComponent,
    EncountersComponent,
    RequestHealthSystemComponent,
    SelectDataSourceComponent,
    ManualMedicalRecordsComponent,
    SharePatientResourceComponent,
    MedicalRecordsDetailsComponent,
  ],
  imports: [CommonModule, PatientRoutingModule],
})
export class PatientModule { }
