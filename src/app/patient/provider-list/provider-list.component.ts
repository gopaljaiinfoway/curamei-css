import { state, style, trigger } from '@angular/animations'
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core'
import { FormBuilder, FormControl, FormGroup } from '@angular/forms'
import { MatDialog, MatDialogConfig } from '@angular/material/dialog'
import { MatSnackBar } from '@angular/material/snack-bar'
import { MatTableDataSource } from '@angular/material/table'
import { Router } from '@angular/router'
import { NotifierService } from 'angular-notifier'
import { BsModalService } from 'ngx-bootstrap/modal'
import { CommmanService } from '../../_services/commman.service'
import { EditProviderDialogComponent } from '../edit-provider-dialog/edit-provider-dialog.component'



@Component({
  selector: 'app-provider-list',
  templateUrl: './provider-list.component.html',
  styleUrls: ['./provider-list.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
    ]),
  ],
})


export class ProviderListComponent implements OnInit {
  users: any = [];
  throughProviderFlag: boolean = false;
  displayedColumns: string[] = [
    'npino',
    'name',
    'state_name',
    'postal_code',
    'addressone',
    // 'addresstwo',
    'taxonomy',
    'action',
    'details',
  ];
  mobileDisplayedColumns: string[] = [
    'description',
    'action',
  ]
  UploadText = '';
  filesToUpload: Array<File> = [];
  action = null
  addFilesForm: FormGroup
  buttonUserText = 'Add Provider'
  expandedElement: any
  providerList: [];
  dataSource: MatTableDataSource<any>;
  noRecordFond = 'Loading...';
  upId: any;
  filleList: any = [];
  filleList1: any = [];
  panelOpenState = false;
  deleteBtn = "Remove";
  archiveBtn = "Archive";
  orgData: any
  orgDataRecord: any
  orgDataRecordTaxo: any;
  particularProviderRecord: object;
  editRowId:any;
  downloadingWait:boolean=false;
  @ViewChild('myInput') myInputVariable: ElementRef;
  filterValue: string


  constructor (private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private modalService: BsModalService,
    private commanService: CommmanService,
    public notifierService: NotifierService,
    private _snackBar: MatSnackBar,
    private router: Router) {
    this.addFilesForm = new FormGroup({
      fileName: new FormControl('')
    });


  }

  ngOnInit(): void {
    if(this.router.url == '/provider/provider-list'){
      this.getAllPatient('Fitbit');
      this.throughProviderFlag = true;
      this.displayedColumns = [
        'npino',
        'name',
        'state_name',
        'postal_code',
        'addressone',
        'taxonomy',
        'details',
      ];
    }
    if(this.router.url == '/patient/provider-list'){
      this.throughProviderFlag = false;
      this.patientProviderList();
    }
  }

  getAllPatient(type){
    this.commanService.getAllPatient(type)
    .subscribe((result: any) => {
      if(result.success){
        this.noRecordFond = 'No any user selected, please select user to view';
        this.users = result.users;
      }
    })
  }

  selectedUser(user_id){
    if(user_id == undefined){
      this.dataSource = new MatTableDataSource([]);
      this.noRecordFond = 'No any user selected, please select user to view';
    }else{
      this.commanService.getPatientProviderListByUser(user_id).subscribe(
        (res: any) => {
          this.providerData(res);
        },
        (err) => {
          console.log(err);
        }
      );
    }
  }

  checkButton() {
    console.log('check-button')
  }

  patientProviderList() {
    this.commanService.getPatientProviderList().subscribe(
      (res: any) => {
        this.providerData(res);
      },
      (err) => {
        console.log(err);
      }
    );
  }
  providerData(res){
    if (res.success && res.data.length > 0) {
      for (let i = 0; i < res.data.length; i++) {
        res.data[i].zipcode = res.data[i].zipcode.substring(0, 6);
      }

      this.dataSource = new MatTableDataSource(res.data);
    } else {
      this.dataSource = new MatTableDataSource([]);
      this.noRecordFond = 'Record is not  available';
    }
  }
  getParticularProviderDetails(id) {
    this.commanService.getProvderFromList(id).subscribe((res: any) => {
      this.particularProviderRecord = res.data[0];
      console.log('**********************************************')
      console.log(this.particularProviderRecord)
      console.log('**********************************************')
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = this.particularProviderRecord;
      dialogConfig.width = "40%";
      dialogConfig.panelClass = "mob-dialog";
      const dialogRef = this.dialog.open(EditProviderDialogComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
        if (result.status) {
          console.log('filter ' + this.filterValue)
          if (this.filterValue === undefined) {
            this.patientProviderList();
          } else if (this.filterValue !== undefined) {

            this.patientProviderList();
          } else {
            this.patientProviderList();
          }
        }
      });

    })
  }

  patientProviderUploadList() {
    this.commanService.getPatientProviderUploadList(this.upId).subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
          this.filleList = res.data;
        } else {
          this.filleList = [];
          this.noRecordFond = 'Record is not  available';
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getproviderId(Id) {
    this.upId = Id;
    this.patientProviderUploadList();
  }

  fileUploadEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }
  upload() {
    const formData: any = new FormData();
    const files: Array<File> = this.filesToUpload;
    if (files.length <= 0) {
      this._snackBar.open('Please first choose files', this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['red-snackbar'],
      });
    } else {
      this.UploadText = "Uploading files...";
      formData.append('upId', this.upId);
      for (let i = 0; i < files.length; i++) {
        formData.append("uploads[]", files[i], files[i]['name']);
      }

      this.commanService.uploadFilesPatient(formData)
        .subscribe((result: any) => {
          this.UploadText = "";
          if (result.success) {
            this.filesToUpload = [];
            this.myInputVariable.nativeElement.value = '';
            this.addFilesForm.reset();
            this._snackBar.open(result.message, this.action, {
              duration: 3000,
              verticalPosition: 'top', // 'top' | 'bottom'
              panelClass: ['green-snackbar'],
            });
            this.patientProviderUploadList();
          } else {
            this._snackBar.open(result.message, this.action, {
              duration: 3000,
              verticalPosition: 'top', // 'top' | 'bottom'
              panelClass: ['red-snackbar'],
            });
          }
        }, err => {
          console.log(err.message);
        })
    }
  }


  ParticularDelete(upiId) {
    if (confirm("Do you want to permanently delete this provider ?")) {
      //  this.deleteBtn = "Wait...";
      this.commanService.deletePatientProvider(upiId).subscribe((res: any) => {
        if (res.success) {
          this.patientProviderList();
          this.deleteBtn = "Remove";
          this._snackBar.open(res.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }

      },
        err => {
          console.log(err.message);
          this.deleteBtn = "Remove";
        })
    }
  }

  deletePatientProviderFile(upiId) {
    if (confirm("Do you want to permanently delete this file ?")) {
      //  this.deleteBtn = "Wait...";
      this.commanService.deletePatientProviderFile(upiId).subscribe((res: any) => {
        this.patientProviderUploadList();
        this.deleteBtn = "Remove";
      },
        err => {
          console.log(err.message);
          this.deleteBtn = "Remove";
        })
    }
  }


  archivePatientProviderFile(upiId) {
    if (confirm("Do you want to permanently archive this file ?")) {
      this.archiveBtn = "Wait...";
      this.commanService.archivePatientProviderFile(upiId).subscribe((res: any) => {
        this.patientProviderUploadList();
        this.archiveBtn = "Archive";
      },
        err => {
          console.log(err.message);
          this.archiveBtn = "Archive";
        })
    }
  }

  applyFilter(event: Event) {
    this.filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = this.filterValue.trim().toLowerCase();
  }

  // applyFilter(data) {
  //   if (data == '' || data == undefined) {
  //     this.patientProviderList();
  //   } else {

  //     this.commanService.filterProviderList(data).subscribe(
  //       (res: any) => {
  //         if (res.success && res.data.length > 0) {
  //           console.log(res.data);
  //           this.dataSource = new MatTableDataSource(res.data);
  //         } else {
  //           this.dataSource = new MatTableDataSource([]);
  //         }
  //       },
  //       (err) => {
  //         console.log(err);
  //       }
  //     );
  //   }
  // }

  downloadDocument(upiId,fileName) {
    this.downloadingWait = true;
      this.commanService.downloadDocument(upiId).subscribe((res: any) => {
        this.downloadingWait = false;
      const blob = new Blob([res]);
        let url = URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', fileName);
        document.body.appendChild(link);
        link.click();
      },
        err => {
          console.log(err.message);
        })
    
  }

  editFileName(id) {
    this.editRowId = id;
    console.log(this.editRowId);
  }

  updateFileNameRow(upiid,value){
    this.editRowId=null;
    this.commanService.updateDocumentFileName({name:value,upiid:upiid}).subscribe((res: any) => {
      if(res.success){
        this.patientProviderUploadList();
        this._snackBar.open(res.message, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
      }else{
        this._snackBar.open(res.message, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      }
    },
      err => {
        console.log(err.message);
      })
  }

}
