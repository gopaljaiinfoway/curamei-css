import { Component, OnInit } from '@angular/core';
import { CommmanService } from 'src/app/_services/commman.service';
import {formatDate} from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import jwt_decode from 'jwt-decode';
import { CookieService } from 'ngx-cookie-service'
@Component({
  selector: 'app-share-patient-resource',
  templateUrl: './share-patient-resource.component.html',
  styleUrls: ['./share-patient-resource.component.css']
})
export class SharePatientResourceComponent implements OnInit {
  noRecordFond:any;
  providerList:any;
  ResouceData:any;
  buttonText="Grant Access";
  buttonTextPermission="Permission";
  action=null;
  noRecordFondResourceSharing:any;
  public patientResourceShareForm: FormGroup;
  dataSharingFlag:boolean=false;
  roleData = [];
  IconName='add';
  loading:any;
  displayedColumns: string[] = ['provider_name', 'resource_name', 'action']
  dataSource: MatTableDataSource<any>;
  roleId:any;
 // dataSourceAppPer: MatTableDataSource<any>;
  constructor(
    private commanService: CommmanService,  private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar, private cookieService: CookieService,
  ) {
    let tokenGet = this.cookieService.get('token')
    if (tokenGet && tokenGet != null) {
      let decoded = jwt_decode(tokenGet, 'llp');
      this.roleId = decoded.role_id;
    }

    this.patientResourceShareForm = this.formBuilder.group({
      providerName: [null, Validators.required],
      ResourceName: [null, Validators.required],
    })

  }

  ngOnInit(): void {
   this.patientProviderList();
   this.patientResourceList();
   this.patientResourceSharing();
  }
  ToggleForm(){
     if(this.dataSharingFlag){
      this.dataSharingFlag=false;
      this.IconName='add';
     }else{
      this.dataSharingFlag=true;
      this.IconName='menu';
     }
     

  }


  onSubmit() {
   this.buttonText = 'Sending...'
    this.commanService.patientResourceSharingAdd(this.patientResourceShareForm.value).subscribe(
      (resmsg: any) => {
             if(resmsg.success){
              this.patientResourceSharing();
              this.buttonText = 'Grant Access'
              this.patientResourceShareForm.reset();
              this._snackBar.open(resmsg.message, this.action, {
                duration: 3000,
                verticalPosition: 'top', // 'top' | 'bottom'
                panelClass: ['green-snackbar'],
              });
             }else {
              this._snackBar.open(resmsg.message, this.action, {
                duration: 3000,
                verticalPosition: 'top', // 'top' | 'bottom'
                panelClass: ['red-snackbar'],
              });
              this.buttonText = 'Grant Access';
            }
      },
      (err) => {
        this.buttonText = 'Grant Access';
      }
    )
  }

  patientProviderList() {
    this.commanService.getPatientProviderList().subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
           this.providerList = res.data;
        } else {
          this.providerList = [];
          this.noRecordFond = 'Record is not  available';
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }
  patientResourceList() {
    this.commanService.getSharingResourceList().subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
           this.ResouceData = res.data;
        } else {
          this.ResouceData = [];
          this.noRecordFond = 'Record is not  available';
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  patientResourceSharing() {
    this.noRecordFondResourceSharing="Loading...";
    this.commanService.getpatientResourceSharing().subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
          this.dataSource = new MatTableDataSource(res.data);
        } else {
          this.dataSource = new MatTableDataSource([]);
          this.noRecordFondResourceSharing = 'Record is not  available';
        }
      },
      (err) => {
        this.noRecordFondResourceSharing = 'Record is not  available';
        console.log(err);
      }
    );
  }

  deletepatientResourceSharing(patProvId) {
    if (confirm("Do you want to permanently delete this resource sharing ?")) {
      this.commanService.deletepatientResourceSharing(patProvId).subscribe((res: any) => {
        if (res.success) {
          this.patientResourceSharing();
          this._snackBar.open(res.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }

      },
        err => {
          console.log(err.message);
        })
    }
  }


}
