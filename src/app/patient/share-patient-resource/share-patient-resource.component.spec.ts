import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharePatientResourceComponent } from './share-patient-resource.component';

describe('SharePatientResourceComponent', () => {
  let component: SharePatientResourceComponent;
  let fixture: ComponentFixture<SharePatientResourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharePatientResourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharePatientResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
