import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalRecordsDetailsComponent } from './medical-records-details.component';

describe('MedicalRecordsDetailsComponent', () => {
  let component: MedicalRecordsDetailsComponent;
  let fixture: ComponentFixture<MedicalRecordsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalRecordsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalRecordsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
