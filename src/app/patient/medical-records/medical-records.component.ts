import { Component, OnInit, Input, ɵConsole } from '@angular/core';
import { CommmanService } from 'src/app/_services/commman.service';
import { records } from './medial-record';
import {formatDate} from '@angular/common';
import { Router } from '@angular/router';
import { MedicalRecordsDetailsComponent} from './medical-records-details/medical-records-details.component';
import {MatDialog,MatDialogRef} from '@angular/material/dialog';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-medical-records',
  templateUrl: './medical-records.component.html',
  styleUrls: ['./medical-records.component.css']
})
export class MedicalRecordsComponent implements OnInit {

  @Input() user_id: any = null;
  @Input() user_type: any;

  
  addFlag: Boolean = false;
  panelOpenState: Boolean;
  medRecord: any;
  medicalRecord: any;
  progessValue:any;
  finalMedRecord: any;
  resourceRecord: any;
  resourceRecordType: any;
  resourceCode: any;
  allresourceCode: any[];
  arr: any[];
  enableLoading:boolean=false;
  dataStatusDispaly='Please wait...';



  constructor(
    private commanService: CommmanService,
    private router: Router, public dialog: MatDialog, private spinner: NgxSpinnerService
  ) {
    this.medicalRecord = [
      {
        key: 1,
        name: "Conditions",
        resorceType: "Condition",
        records: []
      },
      {
        key: 2,
        name: "Medications",
        resorceType: "MedicationStatement",
        records: []
      },
      {
        key: 3,
        name: "Allergies",
        resorceType: "AllergyIntolerance",
        records: []
      },
      {
        key: 4,
        name: "Immunizations",
        resorceType: "Immunization",
        records: []
      },
      {
        key: 6,
        name: "Observations",
        resorceType: "Observation",
        records: []
       
      }, 
      {
        key: 4,
        name: "Family History",
        resorceType: "FamilyMemberHistory",
        records: []
      }, {
        key: 7,
        name: "Diagonostic Reports",
        resorceType: "DiagnosticReport",
        records: []
      }, {
        key: 8,
        name: "Procedures",
        resorceType: "Procedure",
        records: []
      }, {
        key: 9,
        name: "Prescriptions",
        resorceType: "MedicationOrder",
        records: []
      }, {
        key: 10,
        name: "Care Plans",
        resorceType: "CarePlan",
        records: []
      }, 
      {
        key: 11,
        name: "Goals",
        resorceType: "Goal",
        records: []
      },
      {
        key: 12,
        name: "Encounter",
        resorceType: "Encounter",
        records: []
      }
    ];
  }

  ngOnInit(): void {
    this.spinner.show();
    if(this.router.url == '/patient/medical-records'){
      this.addFlag = true;
    }else{
      this.addFlag = false;
    }
    // this.getAllData();

    this.contitionsGCPHealthcareResource('Condition');
    this.allergiesGCPHealthcareResource('AllergyIntolerance');
    this.immunizationsGCPHealthcareResource('Immunization');
    this.observationsGCPHealthcareResource('Observation');
    this.medicationStatementGCPHealthcareResource('MedicationStatement');
    this.familyMemberHistoryGCPHealthcareResource('FamilyMemberHistory');
    this.procedureGCPHealthcareResource('Procedure');
    this.medicationOrderGCPHealthcareResource('MedicationOrder');
    this.goalGCPHealthcareResource('Goal');
    this.carePlanGCPHealthcareResource('CarePlan');
    this.diagnosticReportGCPHealthcareResource('DiagnosticReport');
    this.encounterGCPHealthcareResource('Encounter');
    
    
    
    
  }


  openDialog(resourceData) {
  const dialogRef = this.dialog.open(MedicalRecordsDetailsComponent, {
    minHeight: 'calc(80vh - 90px)',
          height : 'auto',
    width: '600px',
    data: resourceData,
  }

  );
  dialogRef.afterClosed().subscribe(result => {
    this.GCPHealthcareResourceByType(result);
  });
}

  // =======================GOPAL Pull OneUp health resources & push it into GCP healthcare===========================
  getOneUpResourceByType(resourType) {
    this.enableLoading=true;
    this.commanService.getOneUpHealthResource(resourType)
      .subscribe((result: any) => {
        this.enableLoading=false;
        if (result.success) {
          this.GCPHealthcareResourceByType(resourType);
        }else{
          this.dataStatusDispaly=`${resourType} resource data is  not available.`;
        }
      }, (error: any) => {
        console.log(error)
      })
  }

  GCPHealthcareResourceByType(resourType){
    this.dataStatusDispaly='Please wait...';
       if(resourType=='Condition')
       this.contitionsGCPHealthcareResource(resourType);
       else if(resourType=='AllergyIntolerance')
       this.allergiesGCPHealthcareResource(resourType);
       else if(resourType=='Immunization')
       this.immunizationsGCPHealthcareResource(resourType);
       else if(resourType=='Observation')
       this.observationsGCPHealthcareResource(resourType);
       else if(resourType=='MedicationStatement')
       this.medicationStatementGCPHealthcareResource(resourType);
       else if(resourType=='Procedure')
       this.procedureGCPHealthcareResource(resourType);
       else if(resourType=='MedicationOrder')
       this.medicationOrderGCPHealthcareResource(resourType);
       else if(resourType=='FamilyMemberHistory')
       this.familyMemberHistoryGCPHealthcareResource(resourType);
       else if(resourType=='Goal')
       this.goalGCPHealthcareResource(resourType);
       else if(resourType=='CarePlan')
       this.carePlanGCPHealthcareResource('CarePlan');
       else if(resourType=='DiagnosticReport')
       this.diagnosticReportGCPHealthcareResource('DiagnosticReport');
       else if(resourType=='Encounter')
       this.encounterGCPHealthcareResource('Encounter');
       
       
       else
       console.log('-------------------No type match-----------------');
  }


  
  contitionsGCPHealthcareResource(resourType) {
    this.commanService.getContitionsGCPHealthcareResource(resourType)
      .subscribe((result: any) => {
        if (result.success) {
           if(result.data.length > 0){
               this.medicalRecord.forEach(element => {
              if(element.resorceType==resourType){
               element.records = (result.data); 
              }
             });
           }else{
               this.dataStatusDispaly='Condition resource data is  not available.'
           }
        }else{
          this.dataStatusDispaly= `${result.message}`;
        }

      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }


  allergiesGCPHealthcareResource(resourType) {
    this.commanService.getAllergiesGCPHealthcareResource(resourType)
      .subscribe((result: any) => {
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly='Allergies resource data is  not available.'
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }


  immunizationsGCPHealthcareResource(resourType) {
    this.commanService.getImmunizationsGCPHealthcareResource(resourType)
      .subscribe((result: any) => {
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly='Immunizations resource data is  not available.'
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }


  
  observationsGCPHealthcareResource(resourType) {
    this.commanService.getObservationsGCPHealthcareResource(resourType)
      .subscribe((result: any) => {
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly='Observations resource data is  not available.'
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }




  medicationStatementGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`${resourType} resource data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }



  procedureGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`${resourType} resource data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }



  medicationOrderGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`${resourType} resource data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }



  goalGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`${resourType} resource data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }



  familyMemberHistoryGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {

        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`${resourType} resource data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }

  carePlanGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {
        console.log("==================CALL carePlanGCPHealthcareResource-------------------"+resourType);
        console.log(result);
        console.log("==================CALL carePlanGCPHealthcareResource-------------------");
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`${resourType} resource data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }


  diagnosticReportGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {
        console.log("==================CALL diagnosticReportGCPHealthcareResource-------------------"+resourType);
        console.log(result);
        console.log("==================CALL diagnosticReportGCPHealthcareResource-------------------");
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`${resourType} resource data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }

  
  encounterGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {
        console.log("==================CALL encounterGCPHealthcareResource-------------------"+resourType);
        console.log(result);
        console.log("==================CALL encounterGCPHealthcareResource-------------------");
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`${resourType} resource data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }

  // Get GCP healthcare resource by ID & resourceType
  getHealthcareResourcesByID(user_type, user_id, resource_type){
    this.commanService.getHealthData(user_type, user_id, resource_type)
    .subscribe((result: any) => {
      //this.filterHealthcareData(result, resource_type)
    }, (error: any) => {
      console.log(error)
    })
  }
  // Get GCP healthcare resources ================ REMOVE BELOW ALL  AFTER ALL COMPLETED WORK ===========================
  getGCPHealthcareResources(resourType) {
    for (let i = 0; i < 20; i++) {
      this.progessValue = i;
    }
    this.commanService.getGCPHealthResource(resourType)
      .subscribe((result: any) => {
       // this.filterHealthcareData(result, resourType);
        

      }, (error: any) => {
        console.log(error)
      })
  }

}
