import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CommmanService } from './../../_services/commman.service';

@Component({
  selector: 'app-edit-provider-dialog',
  templateUrl: './edit-provider-dialog.component.html',
  styleUrls: ['./edit-provider-dialog.component.css']
})
export class EditProviderDialogComponent implements OnInit {
  editProviderForm: FormGroup;
  providerRecievedRecord: any;
  providerUpdateStatus: boolean;
  durationInSeconds: number = 5;
  action: string;
  firstname: any;
  lastname: any;
  fullname:any;

  constructor (public dialogRef: MatDialogRef<EditProviderDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private formBuilder: FormBuilder, private commanService: CommmanService, private _snackBar: MatSnackBar) {
    this.editProviderForm = new FormGroup({
      npino: new FormControl('', [Validators.required]),
      full_name: new FormControl(''),
      f_name: new FormControl(''),
      l_name: new FormControl(''),
      state_name: new FormControl('', [Validators.required]),
      country_name: new FormControl('', [Validators.required]),
      postal_code: new FormControl('', [Validators.required]),
      addressone: new FormControl('', [Validators.required]),
      addresstwo: new FormControl(''),
      taxonomy: new FormControl('', [Validators.required]),
    })
    console.log(data.last_name);
    this.lastname = data.last_name;
    this.fullname = data.full_name;
  }

  ngOnInit(): void {
    this.providerRecievedRecord = this.data;
    console.log(this.providerRecievedRecord)


    this.editProviderForm.patchValue({
      npino: this.providerRecievedRecord.npi,
      full_name: this.providerRecievedRecord.full_name,
      f_name: this.providerRecievedRecord.first_name,
      l_name: this.providerRecievedRecord.last_name,
      state_name: this.providerRecievedRecord.state_id,
      country_name: this.providerRecievedRecord.country,
      postal_code: this.providerRecievedRecord.zipcode,
      addressone: this.providerRecievedRecord.address1,
      addresstwo: this.providerRecievedRecord.address2,
      taxonomy: this.providerRecievedRecord.taxonomy
    });
  }

  updateProvider() {
    this.commanService.updateProvider(this.editProviderForm.value, this.providerRecievedRecord.pid).subscribe((res: any) => {
      if (res.success) {
        this.providerUpdateStatus = true;
        this.dialogRef.close({ status: this.providerUpdateStatus });
        this._snackBar.open(res.message, this.action, {
          duration: 3000,
          horizontalPosition: 'right',
          verticalPosition: 'bottom',
          panelClass: ['green-snackbar'],
        });
      } else {
        this.providerUpdateStatus = false;
      }
    })
  }

  close() {
    this.dialogRef.close();
  }

}
