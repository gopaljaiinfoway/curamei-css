import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Enrollment4Component } from './enrollment4.component';

describe('Enrollment4Component', () => {
  let component: Enrollment4Component;
  let fixture: ComponentFixture<Enrollment4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Enrollment4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Enrollment4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
