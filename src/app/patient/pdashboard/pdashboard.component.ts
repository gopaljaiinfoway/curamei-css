import { state, style, trigger } from '@angular/animations'
import { Component, OnInit } from '@angular/core'
import { FormControl, Validators } from '@angular/forms'
import { MatTableDataSource } from '@angular/material/table';
import { CommmanService } from 'src/app/_services/commman.service'
declare var $: any

@Component({
  selector: 'app-pdashboard',
  templateUrl: './pdashboard.component.html',
  styleUrls: ['./pdashboard.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      // transition(
      //   'expanded <=> collapsed',
      //   animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      // ),
    ]),
  ],
})
export class PdashboardComponent implements OnInit {
  constructor(private commanService: CommmanService) {}
 
  displayedColumns: string[] = ['type', 'name', 'email',  'created', 'details']
  dataSource: MatTableDataSource<any>;
  panelOpenState = false
  expandedElement: any;
  public icon = 'add';
  emailFilter:'';
  dataFilterLoading:any;
  public toggleIcon() {
    if (this.icon === 'add') {
      this.icon = 'remove'
    } else {
      this.icon = 'add'
    }
  }

  ngOnInit(): void {
    this.sidebarCollapse();
    this.adminDashboardList();
  }
  sidebarCollapse() {
    $('#sidebar').toggleClass('active')
    $('#body').toggleClass('active')
  }

  adminDashboardList(){
    this.dataFilterLoading='Loading...';
    this.commanService.adminDashboardList().subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
          console.log(res.data);
          this.dataSource = new MatTableDataSource(res.data);
        } else {
          this.dataSource = new MatTableDataSource([]);
          this.dataFilterLoading='Record is not available.';
        }
      },
      (err) => {
        console.log(err);
        this.dataFilterLoading='Record is not available.';
      }
    );
  }

  applyFilter() {
    if (this.emailFilter == '' || this.emailFilter == undefined) {
     this.adminDashboardList();
    } else {

      this.dataFilterLoading='Loading...';
      this.commanService.adminUserGetInfoByEmail(this.emailFilter).subscribe(
        (res: any) => {
          console.log('===========================================================');
          console.log(res)
          if (res.success && res.data.length > 0) {
            this.dataSource = new MatTableDataSource(res.data);
          } else {
            this.dataSource = new MatTableDataSource([]);
            this.dataFilterLoading='Record is not available.';
          }
        },
        (err) => {
          console.log(err);
          this.dataFilterLoading='Record is not available.';
        }
      );
    }
  }

}

