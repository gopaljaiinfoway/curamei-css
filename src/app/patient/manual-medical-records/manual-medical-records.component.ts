import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CommmanService } from 'src/app/_services/commman.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-manual-medical-records',
  templateUrl: './manual-medical-records.component.html',
  styleUrls: ['./manual-medical-records.component.css']
})
export class ManualMedicalRecordsComponent implements OnInit {
  addFlag: Boolean = false;
  ifManualPatientExist = false;
  panelOpenState: Boolean;
  type: any;
  pType = new FormControl('');
  selectedType = '';
  encounters = [{id: 123, name: 'encounter 1'},{id: 1234, name: 'encounter 2'}];
  immnuization_Disease = [{id: 123, name: 'AAA'},{id:1234, name:'BBB'}];
  genders = [{ id: 1, name: 'male' }, { id: 2, name: 'female' }, { id: 3, name: 'others' }];
  martial_status = [{ id: 1, name: 'single' }, { id: 1, name: 'married' }, { id: 1, name: 'other' }];
  allergy_category = [{ id: 1, name: 'food' }, { id: 2, name: 'medication' }, { id: 3, name: 'environment' }, { id: 4, name: 'biologic' }];
  allergy_status = [{ id: 1, name: 'active' }, { id: 2, name: 'inactive' }, { id: 3, name: 'resolved' }];
  allergy_criticality = [{ id: 1, name: 'CRITL' }];
  observation_status = [{ id: 1, name: 'registered' }, { id: 2, name: 'preliminary' }, { id: 3, name: 'final' }, { id: 4, name: 'amended' }];
  immnuization_status = [{ id: 1, name: 'completed' }, { id: 2, name: 'entered-in-error' }, { id: 3, name: 'not-done' }];
  immnuization_wasNotGiven = [{ id: 1, name: true }, { id: 2, name: false }];
  immnuization_reported = [{ id: 1, name: true }, { id: 2, name: false }];
  medication_status = [{ id: 1, name: 'active' }, { id: 2, name: 'completed' }, { id: 3, name: 'entered-in-error' }, { id: 3, name: 'intended' }, { id: 3, name: 'stopped' }, { id: 3, name: 'on-hold' }];
  medication_timing = [{ id: 1, name: '1:00' }, { id: 2, name: '2:00' }, { id: 3, name: '3:00' }, { id: 4, name: '4:00' }, { id: 5, name: '5:00' }, { id: 6, name: '6:00' }, { id: 7, name: '7:00' }, { id: 8, name: '8:00' }, { id: 9, name: '9:00' }, { id: 10, name: '10:00' }, { id: 11, name: '11:00' }, { id: 12, name: '12:00' }];
  medication_meridian = [{ id: 1, name: 'AM' }, { id: 2, name: 'PM' }];
  medication_needed = [{ id: 1, name: true }, { id: 2, name: false }];
  condition_category = [{ id: 1, name: "problem-list-item" }, { id: 2, name: "encounter-diagnosis" }]; // problem-list-item | encounter-diagnosis
  condition_status = [{ id: 1, name: 'active' }, { id: 2, name: 'recurrence' }, { id: 3, name: 'relapse' }, { id: 4, name: 'inactive' }, { id: 4, name: 'remission' }, { id: 4, name: 'resolved' }];
  condition_verification = [{ id: 1, name: 'unconfirmed' }, { id: 2, name: 'provisional' }, { id: 3, name: 'differential' }, { id: 4, name: 'confirmed' }, { id: 4, name: 'refuted' }, { id: 4, name: 'entered-in-error' }];
  newPatientForm: FormGroup;
  allergyForm: FormGroup;
  observationForm: FormGroup;
  immunizationsForm: FormGroup;
  medicationForm: FormGroup;
  conditionForm: FormGroup;
  containers = [];
  MedicationStatementContainers = [];
  AllergyIntoleranceContainers = [];
  ImmunizationContainers = [];
  allergyIntoleranceForm: FormControl;
  immunizationForm: FormControl;
  medicationStatementForm: FormGroup;
  patientResourceType: string = "Patient";
  allergyResourceType: string = "AllergyIntolerance";
  observationResourceType: string = "Observation";
  immunizationsResourceType: string = "Immunization";
  medicationResourceType: string = "MedicationStatement";
  condtionResourceType: string = "Condition";
  patientStatus: Boolean = true;
  clinicalStatus: Boolean;
  abatement: any;
  allergySubstance: any;
  allergyCategory: any;
  allergyStatus: any;
  allergyType: any;
  observation_bodySite: any;
  observationEncounter: any;
  observation_absent_reason: any;
  observation_reference_range: any;
  observation_comments: any;
  immunizationRoute: any;
  immunizationSite: any;
  immunizationDisease: any;
  observation_method: any;
  allergyNote: any;
  allergyCriticality: any;
  obsevationCode: any;
  observationCategory: any;
  observationStatus: any;
  observationDateTime: any;
  observationQuantity: any;
  immunizationVaccineCode: any;
  immunizationStatus: any;
  immunizationWasNotGiven: any;
  immunizationDate: any;
  immunizationReported: any;
  medicationsMedication: any;
  medicationsDosage: any;
  medicationsStatus: any;
  medicationsDateAsserted: any;
  medicationsReasonForUse: any;
  medicationsDosageTiming: any;
  medicationsDosageTimingMeridian: any;
  medicationsDaosageNeeded: any;
  medicationsDosageMethod: any;
  medicationsDosageQuantity: any;
  medicationsDosageNote: any;
  medicationsDosageUnit: any;
  condtionCode: any;
  condtionCategory: any;
  condtionStatus: any;
  condtionVerificationStatus: any;
  condtionStageSummary: any;
  patientName: any;
  patientGender: any;
  patientBirthDate: any;
  patientMartialStatus: any;
  patientAddress: any;
  progessValue: any = 0;

  constructor (
    private formBuilder: FormBuilder,
    private commanService: CommmanService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private notifier: NotifierService,
    ) {
    this.newPatientForm = this.formBuilder.group({
      name: '',
      // contact: '',
      gender: '',
      birthDate: '',
      maritalStatus: '',
      address: '',
      resourceType: '',
      active: ''
    });
    this.conditionForm = this.formBuilder.group({
      code: '',
      category: '',
      status: '',
      verificationStatus: '',
      stageSummary: '',
      resourceType: '',
      abatement: ''
    });
    this.allergyForm = this.formBuilder.group({
      substance: '',
      category: '',
      status: '',
      criticality: '',
      type: '',
      note: '',
      resourceType: '',
    });
    this.observationForm = this.formBuilder.group({
      code: '',
      category: '',
      status: '',
      body_site: '',
      method: '',
      encounter: '',
      absent_reason:'',
      reference_range:'',
      comment:'',
      // effectiveDateTime: '',
      quantity: '',
      resourceType: '',
    });
    this.immunizationsForm = this.formBuilder.group({
      vaccineCode: '',
      date: '',
      status: '',
      // wasNotGiven: '',
      route: '',
      site: '',
      disease: '',
      resourceType: '',
    });
    this.medicationForm = this.formBuilder.group({
      medication: '',
      dosage: '',
      status: '',
      dateAsserted: '',
      reasonForUse: '',
      dosageTiming: '',
      dosageTimingMeridian: '',
      daosageNeeded: '',
      dosageMethod: '',
      dosageQuantity: '',
      dosageNote: '',
      dosageUnit: '',
      resourceType: '',
    });
    // this.conditionForm = this.formBuilder.group({
    //   code: '',
    //   category: '',
    //   fields: this.formBuilder.array([])
    //   // clinical_status: new FormControl(''),
    //   // verification_status: new FormControl(''),
    //   // stage_summary: new FormControl(''),
    // });
    this.medicationStatementForm = new FormGroup({
      medication: new FormControl(''),
      dosage_text: new FormControl(''),
      clinical_status: new FormControl(''),
      date_asserted: new FormControl(''),
      reason: new FormControl(''),
      dosage_timing: new FormControl(''),
      dosage_as_needed: new FormControl(''),
      dosage_method: new FormControl(''),
      dosage_quantity: new FormControl(''),
      note: new FormControl(''),
    });
    this.allergyIntoleranceForm = new FormControl({
      substance: new FormControl(''),
      category: new FormControl(''),
      clinical_status: new FormControl(''),
      type: new FormControl(''),
      critically: new FormControl(''),
      note: new FormControl(''),
    });
    this.immunizationForm = new FormControl({
      vaccine_code: new FormControl(''),
      explanation_reason: new FormControl(''),
      clinical_status: new FormControl(''),
      date_administered: new FormControl(''),
      target_diseases: new FormControl('')
    });
  }

  fields(): FormArray {
    return this.conditionForm.get('fields') as FormArray;
  }

  newField(): FormGroup {
    return this.formBuilder.group({
      clinical_status: '',
      verification_status: '',
      stage_summary: '',
    });
  }

  addField() {
    this.fields().push(this.newField());
  }

  removeField(i: number) {
    this.fields().removeAt(i);
  }

  ngOnInit(): void {
    if(this.router.url == '/patient/manual-medical-records'){
      this.addFlag = true;
    }else{
      this.addFlag = false;
    }
    this.checkPatientIfExist('Manual');
    // this.getGCPHealthcareResources(this.allergyResourceType);
  };
  checkPatientIfExist(type){
    this.commanService.checkAnyPatientIfExist(type)
    .subscribe((result: any) => {
      if(result.success){
        this.ifManualPatientExist = true;
      }else{
        this.ifManualPatientExist = false;
      }
    })
  }

  /**
   * Create New patient
   */

  createNewPatient() {
    this.newPatientForm.controls.active.setValue(this.patientStatus);
    this.newPatientForm.controls.resourceType.setValue(this.patientResourceType);

    let formValue = this.newPatientForm.value;
    this.commanService.createNewResource(formValue).subscribe((data) => {
      console.log('Patient Added Successfully');
    });
  }


  /**
   * Create Allergy Resource
   */

  addAllergyRecord() {
    this.allergyForm.controls.resourceType.setValue(this.allergyResourceType);
    let formValue = this.allergyForm.value;
    console.log(formValue);
    this.commanService.createNewResource(formValue).subscribe((data) => {
      console.log('Allergy Added Successfully');
    });
  }

  /**
   * Create Observation Resource
   */

  addObservationRecord() {
    this.observationForm.controls.resourceType.setValue(this.observationResourceType);
    let formValue = this.observationForm.value;
    console.log(formValue);
    this.commanService.createNewResource(formValue).subscribe((data) => {
      console.log('Observation Added Successfully');
    });
  }

  /**
   * Create Immunization Resource
   */

  addImmunizationRecord() {
    this.immunizationsForm.controls.resourceType.setValue(this.immunizationsResourceType);
    let formValue = this.immunizationsForm.value;
    console.log(formValue);
    this.commanService.createNewResource(formValue).subscribe((data) => {
      console.log('Immunization Added Successfully');
    });
  }


  /**
   * Create Medication Resource
   */
  addMedicationRecord() {
    this.medicationForm.controls.resourceType.setValue(this.medicationResourceType);
    let formValue = this.medicationForm.value;
    console.log(formValue);
    this.commanService.createNewResource(formValue).subscribe((data) => {
      console.log('Medication Added Successfully');
    });
  }

  /**
   * Create Condition Resource
   */

  addConditionRecord() {
    this.conditionForm.controls.resourceType.setValue(this.condtionResourceType);
    let formValue = this.conditionForm.value;
    this.spinner.show();
    this.commanService.createNewResource(formValue)
    .subscribe((result:any) => {
      console.log(result)
      this.spinner.hide()
      if(result.success){
        this.notifier.notify('success', result.message)
      }else{
        this.notifier.notify('error', result.message)
      }
    },(res: any) => {
      this.spinner.hide()
    })
  }

  /**
   * Get Resource Data
  */

  // getGCPHealthcareResources(resourType) {
  //   for (let i = 0; i < 20; i++) {
  //     this.progessValue = i;
  //   }
    
  //   this.commanService.getManualGCPHealthResource(resourType,'Manual')
  //     .subscribe((result: any) => {
  //       for (let i = 0; i < 90; i++) {
  //         this.progessValue = i;
  //       }
  //       this.progessValue = 100;
  //       if (resourType == 'Patient') {
  //         let recordData = result.data;
  //         this.patientName = recordData.name[0].family;
  //         this.patientGender = recordData.gender;
  //         this.patientBirthDate = recordData.birthDate;
  //         this.patientMartialStatus = recordData.maritalStatus.coding[0].code;
  //         this.patientAddress = recordData.address[0].text;

  //       }

  //       if (resourType == 'Observation') {
  //         let recordData = result.data;
  //         console.log(recordData)
  //         // this.obsevationCode = recordData.code.coding[0].code;
  //         this.observationCategory = recordData.category.coding[0].code;
  //         this.observationStatus = recordData.fieldRecords[1].value;
  //         this.observationDateTime = recordData.fieldRecords[2].value;
  //         this.observationQuantity = recordData.fieldRecords[0].value;
  //       }

  //       if (resourType == 'AllergyIntolerance') {
  //         console.log(result.data);
  //         let recordData = result.data;
  //         this.allergySubstance = recordData.substance.text;
  //         this.allergyCategory = recordData.category;
  //         this.allergyStatus = recordData.status;
  //         this.allergyCriticality = recordData.criticality;
  //         this.allergyResourceType = recordData.resourceType;
  //       }

  //       if (resourType == 'Immunization') {
  //         let recordData = result.data;
  //         this.immunizationVaccineCode = recordData.vaccineCode.id;
  //         this.immunizationStatus = recordData.status;
  //         this.immunizationWasNotGiven = JSON.stringify(recordData.wasNotGiven);
  //         this.immunizationDate = recordData.expirationDate;
  //         this.immunizationReported = JSON.stringify(recordData.reported);
  //       }

  //       if (resourType == 'MedicationStatement') {
  //         let recordData = result.data;
  //         this.medicationsMedication = recordData.medicationCodeableConcept.coding[0].code;
  //         this.medicationsDosage = recordData.medicationCodeableConcept.text;
  //         this.medicationsStatus = recordData.status;
  //         this.medicationsDateAsserted = recordData.dateAsserted;
  //         this.medicationsReasonForUse = recordData.reasonForUseCodeableConcept.coding[0].display;
  //         this.medicationsDosageTiming = recordData.dosage[0].timing.code.coding[0].display;
  //         this.medicationsDosageTimingMeridian = recordData.dosage[0].timing.code.coding[0].code;
  //         this.medicationsDaosageNeeded = JSON.stringify(recordData.dosage[0].asNeededBoolean);
  //         this.medicationsDosageMethod = recordData.dosage[0].method.text;
  //         this.medicationsDosageQuantity = recordData.dosage[0].quantityQuantity.value;
  //         this.medicationsDosageNote = recordData.note;
  //         this.medicationsDosageUnit = recordData.dosage[0].quantityQuantity.unit;
  //       }

  //       if (resourType == 'Condition') {
  //         let recordData = result.data;
  //         this.condtionCode = recordData.code.coding[0].code;
  //         this.condtionCategory = recordData.category.coding[0].code;
  //         this.condtionStatus = recordData.clinicalStatus;
  //         this.condtionVerificationStatus = recordData.verificationStatus;
  //         this.condtionStageSummary = recordData.stage.summary.coding[0].display;
  //       }

  //     }, (error: any) => {
  //       console.log(error);
  //     });

  // }



}
