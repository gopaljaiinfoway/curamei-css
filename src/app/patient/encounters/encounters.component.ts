import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-encounters',
  templateUrl: './encounters.component.html',
  styleUrls: ['./encounters.component.css']
})
export class EncountersComponent implements OnInit {
  panelOpenState: boolean;
  encountersRecord = [{
    key: 1,
    title: "primary",
    subtitle: "secondary",
    fieldRecords: [{
      key: 1,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }],
  }, {
    key: 2,
    title: "primary",
    subtitle: "secondary",
    fieldRecords: [{
      key: 1,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }],
  }, {
    key: 3,
    title: "primary",
    subtitle: "secondary",
    fieldRecords: [{
      key: 1,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }],
  }, {
    key: 4,
    title: "primary",
    subtitle: "secondary",
    fieldRecords: [{
      key: 1,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }],
  }, {
    key: 5,
    title: "primary",
    subtitle: "secondary",
    fieldRecords: [{
      key: 1,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }, {
      key: 2,
      name: 'Field Display Name',
      value: 'Value'
    }],
  }
  ]

  constructor () { }

  ngOnInit(): void {
  }

}
