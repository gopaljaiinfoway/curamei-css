import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { CommmanService} from '../../_services/commman.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { NotifierService } from 'angular-notifier';
import { CustomValidators } from 'ng2-validation';
@Component({
  selector: 'app-enrollment',
  templateUrl: './enrollment.component.html',
  styleUrls: ['./enrollment.component.css']
})
export class EnrollmentComponent implements OnInit {
  UserProfileFlag=true;
  UserProfileBtn="Save";
  public userProfileForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private commanService: CommmanService,
     private cookieService: CookieService,notifierService: NotifierService, private router: Router) {
     
  
   }
 
   ngOnInit(): void {
 
   }
 
   onSubmitNext(){
   this.router.navigateByUrl('/patient/enrollment1');

   }

   RadioClick1(){
    this.router.navigateByUrl('/patient/enrollment');
   }
   RadioClick2(){
   this.router.navigateByUrl('/patient/enrollment1');
  }

  RadioClick3(){

   this.router.navigateByUrl('/patient/enrollment2');
  }

  RadioClick4(){
   this.router.navigateByUrl('/patient/enrollment3');
  }

  RadioClick5(){
   this.router.navigateByUrl('/patient/enrollment4');
  }

}
