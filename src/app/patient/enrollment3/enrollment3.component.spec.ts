import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Enrollment3Component } from './enrollment3.component';

describe('Enrollment3Component', () => {
  let component: Enrollment3Component;
  let fixture: ComponentFixture<Enrollment3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Enrollment3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Enrollment3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
