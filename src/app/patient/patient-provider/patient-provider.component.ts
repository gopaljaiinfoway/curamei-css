import { state, style, trigger } from '@angular/animations'
import { Component, OnInit, ɵConsole } from '@angular/core'
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms'
import { ErrorStateMatcher } from '@angular/material/core'
import { MatDialog } from '@angular/material/dialog'
import { MatSnackBar } from '@angular/material/snack-bar'
import { MatTableDataSource } from '@angular/material/table'
import { Router } from '@angular/router'
import { NotifierService } from 'angular-notifier'
import { BsModalService } from 'ngx-bootstrap/modal'
import { CommmanService } from '../../_services/commman.service'
declare var $: any

@Component({
  selector: 'app-patient-provider',
  templateUrl: './patient-provider.component.html',
  styleUrls: ['./patient-provider.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
    ]),
  ],
})
export class PatientProviderComponent implements OnInit {
  // dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA)
  displayedColumns: string[] = [
    'npino',
    'name',
    'state_name',
    'country_name',
    'postal_code',
    'addressone',
    'addresstwo',
    'taxonomy',
    'details',
  ]
  UploadText = 'Upload';
  filesToUpload: Array<File> = [];
  action = null
  addUserForm: FormGroup
  buttonUserText = 'Add Provider'
  expandedElement: any
  providerList: [];
  dataSource: MatTableDataSource<any>;
  noRecordFond = 'Loading...';
  pId: any;
  filleList: any = [];
  filleList1: any = [];
  panelOpenState = false;
  deleteBtn = "Remove";
  archiveBtn = "Archive";
  orgData: any
  orgDataRecord: any
  orgDataRecordTaxo: any;
  matcher = new MyErrorStateMatcher();
  orgDataRecordAddressOne: any
  orgDataRecordAddressTwo: any
  orgDataRecordMedicaidDesc: any
  orgDataRecordMedicaidState: any
  orgDataRecordMedicaidIdentifier: any
  constructor (
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private modalService: BsModalService,
    private commanService: CommmanService,
    public notifierService: NotifierService,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {

    console.log(this.router.getCurrentNavigation().extras.state);
    this.orgData = history.state;
    console.log(this.orgData)
    this.orgDataRecord = this.orgData.data;

  console.log("-----------99999999999999-----------");
  console.log(this.orgDataRecord);
  console.log("------------9999999999999999999999999999---------------");

    for (let i = 0; i < this.orgDataRecord.identifiers.length; i++) {
      console.log(this.orgDataRecord.identifiers[i].desc)
      if (this.orgDataRecord.identifiers[i].desc == 'MEDICAID') {
        this.orgDataRecordMedicaidDesc = this.orgDataRecord.identifiers[i].desc;
        this.orgDataRecordMedicaidState = this.orgDataRecord.identifiers[i].state;
        this.orgDataRecordMedicaidIdentifier = this.orgDataRecord.identifiers[i].identifier;
      } else {
        this.orgDataRecord.identifiers[i].desc = ''
      }
    }

    for (let i = 0; i < this.orgDataRecord.taxonomies.length; i++) {
      if (this.orgDataRecord.taxonomies[i].primary) {
        this.orgDataRecordTaxo = this.orgDataRecord.taxonomies[i].desc;
      } else {
        this.orgDataRecord.taxonomies[i].desc = ''
      }
    }

    for (let i = 0; i < this.orgDataRecord.addresses.length; i++) {
      if (this.orgDataRecord.addresses[i].address_purpose == 'LOCATION') {
        this.orgDataRecordAddressOne = this.orgDataRecord.addresses[i].address_1;
        this.orgDataRecordAddressTwo = this.orgDataRecord.addresses[i].address_2;
      } else {
        this.orgDataRecord.addresses[i].orgDataRecordAddressOne = ''
        this.orgDataRecord.addresses[i].orgDataRecordAddressTwo = ''
      }
    }


    this.addUserForm = new FormGroup({
      npino: new FormControl('', [Validators.required]),
      f_name: new FormControl('', [Validators.required]),
      l_name: new FormControl(''),
      state_name: new FormControl('', [Validators.required]),
      country_name: new FormControl('USA'),
      postal_code: new FormControl('', [Validators.required]),
      addressone: new FormControl('', [Validators.required]),
      addresstwo: new FormControl(''),
      taxonomy: new FormControl('', [Validators.required]),
      medicaid_desc: new FormControl(''),
      medicaid_identifier: new FormControl(''),
      medicaid_state: new FormControl(''),
    })

  }

  ngOnInit(): void {
    this.patientProviderList();
  }



  patientProviderList() {
    this.commanService.getPatientProviderList().subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
          this.dataSource = new MatTableDataSource(res.data);
        } else {
          this.dataSource = new MatTableDataSource([]);
          this.noRecordFond = 'Record is not  available';
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }




  patientProviderUploadList() {
    this.commanService.getPatientProviderUploadList(this.pId).subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
          this.filleList = res.data;
        } else {
          this.filleList = [];
          this.noRecordFond = 'Record is not  available';
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }


  //contact operation start
  addProvider(formDirective: FormGroupDirective) {
    this.buttonUserText = 'Loading....';
    if (!this.addUserForm.invalid) {
       if(this.orgDataRecord.enumeration_type=='NPI-2'){
     this.addUserForm.value.enumeration_type=this.orgDataRecord.enumeration_type;
     this.addUserForm.value.organization_name=this.orgDataRecord.basic.organization_name;
       }else{
        this.addUserForm.value.enumeration_type=this.orgDataRecord.enumeration_type;
       }
      this.commanService.patientProviderAdd(this.addUserForm.value).subscribe(
        (res: any) => {
          if (res.success) {
            this._snackBar.open(res.message, this.action, {
              duration: 3000,
              verticalPosition: 'top', // 'top' | 'bottom'
              panelClass: ['green-snackbar'],
            });
            this.buttonUserText = 'Add Provider';
            this.patientProviderList();
            formDirective.resetForm();
            this.addUserForm.reset();
            this.router.navigate(['/patient/provider-list'])
          } else {
            this._snackBar.open(res.message, this.action, {
              duration: 3000,
              verticalPosition: 'top', // 'top' | 'bottom'
              panelClass: ['red-snackbar'],
            });
            this.buttonUserText = 'Add Provider';

          }
        },
        (err) => {
          console.log(err);
          this.buttonUserText = 'Add Provider';
          // this.serverErrorMessages = err.error.message;
        }
      );
    }
  }


  getproviderId(Id) {
    this.pId = Id;
    this.patientProviderUploadList();
  }


  fileUploadEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }
  upload() {
    const formData: any = new FormData();
    const files: Array<File> = this.filesToUpload;
    if (files.length <= 0) {
      this._snackBar.open('Please first choose files', this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['red-snackbar'],
      });
    } else {
      this.UploadText = "Loading...";
      formData.append('pId', this.pId);
      for (let i = 0; i < files.length; i++) {
        formData.append("uploads[]", files[i], files[i]['name']);
      }

      this.commanService.uploadFilesPatient(formData)
        .subscribe((result: any) => {
          this.UploadText = "Upload";
          if (result.success) {
            this.filesToUpload = [];

            this._snackBar.open(result.message, this.action, {
              duration: 3000,
              verticalPosition: 'top', // 'top' | 'bottom'
              panelClass: ['green-snackbar'],
            });
            this.patientProviderUploadList();
          } else {
            this._snackBar.open(result.message, this.action, {
              duration: 3000,
              verticalPosition: 'top', // 'top' | 'bottom'
              panelClass: ['red-snackbar'],
            });
          }
        }, err => {
          console.log(err.message);
        })
    }
  }



  deletePatientProviderFile(upiId) {
    if (confirm("Do you want to permanently delete this file ?")) {
      this.deleteBtn = "Wait...";
      this.commanService.deletePatientProviderFile(upiId).subscribe((res: any) => {
        this.patientProviderUploadList();
        this.deleteBtn = "Remove";
      },
        err => {
          console.log(err.message);
          this.deleteBtn = "Remove";
        })
    }
  }



  archivePatientProviderFile(upiId) {
    if (confirm("Do you want to permanently archive this file ?")) {
      this.archiveBtn = "Wait...";
      this.commanService.archivePatientProviderFile(upiId).subscribe((res: any) => {
        this.patientProviderUploadList();
        this.archiveBtn = "Archive";
      },
        err => {
          console.log(err.message);
          this.archiveBtn = "Archive";
        })
    }
  }





}



export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

