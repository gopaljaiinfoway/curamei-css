import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientProviderComponent } from './patient-provider.component';

describe('PatientProviderComponent', () => {
  let component: PatientProviderComponent;
  let fixture: ComponentFixture<PatientProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
