import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { CommmanService } from 'src/app/_services/commman.service';

@Component({
  selector: 'app-request-health-system',
  templateUrl: './request-health-system.component.html',
  styleUrls: ['./request-health-system.component.css']
})
export class RequestHealthSystemComponent implements OnInit {
  organisationData: any;
  records: [] = [];
  returnedArray = [];
  finalRecord: number;
  p: number = 1;
  panelOpenState = false;
  particularOrgInfo: any;
  oneUpUserId: any;
  userobj: any;
  providerList: any;
  searchHealthSystemFlag: boolean;
  showForIndividual: boolean;
  showFororganisation: boolean;
  orgnamevalue: any;
  individualData: Object;
  npivalue: string;
  SearchForm: FormGroup;
  action = null;
  dataNotFoundMessage:any;


  constructor (private commanService: CommmanService, private router: Router, private _snackBar: MatSnackBar, ) {
    this.userobj = JSON.parse(localStorage.getItem('user'));
    this.oneUpUserId = this.userobj.oneup_user_id;
  }

  ngOnInit(): void {
    this.SearchForm = new FormGroup({
      firstname: new FormControl(''),
      lastname: new FormControl(''),
      organisationname: new FormControl('')
    })

    this.getHealthOrganisationDefault();
    this.PatientProviderwithFileList();
    this.checkAccessToken();
    this.searchHealthSystemFlag = false;
  }

  checkAccessToken() {
    this.commanService.checkAccessTokenExists(this.oneUpUserId).subscribe((resObj: any) => {
      if (resObj.success) {
        this.userobj.oneup_user_id = resObj.data.oneup_user_id;
        this.userobj.refresh_token = resObj.data.refresh_token;
        this.userobj.access_token = resObj.data.access_token;
        localStorage.setItem('user', JSON.stringify(this.userobj));
      }
    })
  }

  getHealthOrganisationDefault() {
    this.dataNotFoundMessage="Loading...";
    this.commanService.getHealthSystemNameDefault().subscribe((data) => {
      this.organisationData = data
      // console.log(data)
      const objects = Object.keys(data).map((key) => data[key])
      this.records = objects[1]
      this.finalRecord = this.records.length;
      if(this.finalRecord){
        this.dataNotFoundMessage='';
        this.records.forEach((record) => {
          console.log(record)
        })
      }else{
        this.records=[];
        this.dataNotFoundMessage="Data is not available.";
      }
      
    })
  }

  PatientProviderwithFileList() {
    this.commanService.getPatientProviderwithFileList().subscribe((res: any) => {
      if (res.success && res.data.length > 0) {
        this.providerList = res.data;
      } else {
        this.providerList = [];
      }
    })
  }

  getHealthOrganisation() {
    let orgname = this.SearchForm.get('organisationname').value;
    this.commanService.getHealthSystemName(this.npivalue, orgname).subscribe((data) => {
      this.organisationData = data
      // console.log(data)
      const objects = Object.keys(data).map((key) => data[key])
      this.records = objects[1]
      this.finalRecord = this.records.length
      if(this.finalRecord){
        this.dataNotFoundMessage='';
        this.records.forEach((record) => {
          console.log(record)
        })
      }else{
        this.records=[];
        this.dataNotFoundMessage="Data is not available.";
      }





    })
  }

  getHealthIndividual() {
    console.log('clicked')
    let firstname = this.SearchForm.get('firstname').value;
    let lastname = this.SearchForm.get('lastname').value;
    if (firstname != '' && lastname === '') {
      this.commanService.getHealthSystemFirstName(this.npivalue, firstname).subscribe((data) => {
        this.individualData = data

        const objects = Object.keys(data).map((key) => data[key])
        console.log(objects)
        this.records = objects[1]

        this.finalRecord = this.records.length
              
        if(this.finalRecord){
          this.dataNotFoundMessage='';
          this.records.forEach((record) => {
            console.log(record)
          })
        }else{
          this.records=[];
          this.dataNotFoundMessage="Data is not available.";
        }
  
      });
    }
    else if (firstname === '' && lastname != '') {
      this.commanService.getHealthSystemLastName(this.npivalue, lastname).subscribe((data) => {
        this.individualData = data
        // console.log(data)
        const objects = Object.keys(data).map((key) => data[key])
        this.records = objects[1]
        this.finalRecord = this.records.length
        if(this.finalRecord){
          this.dataNotFoundMessage='';
          this.records.forEach((record) => {
            console.log(record)
          })
        }else{
          this.records=[];
          this.dataNotFoundMessage="Data is not available.";
        }
  
      });
    }
    else if (firstname != '' && lastname != '') {
      this.commanService.getHealthSystemBothName(firstname, lastname, this.npivalue).subscribe((data) => {
        this.individualData = data
        console.log(data)
        const objects = Object.keys(data).map((key) => data[key])
        this.records = objects[1]
        this.finalRecord = this.records.length
        if(this.finalRecord){
          this.dataNotFoundMessage='';
          this.records.forEach((record) => {
            console.log(record)
          })
        }else{
          this.records=[];
          this.dataNotFoundMessage="Data is not available.";
        }
      });
    }

  }



  getProviderData(value: number) {
    this.commanService.getProvidertHealthSystem(value).subscribe((data: any) => {
      this.particularOrgInfo = data;
      console.log("--------000000----------------")
      console.log(this.particularOrgInfo)
      console.log("--------000000----------------")
      let dataNpiValue = (this.particularOrgInfo.data[0].number).toString();
      this.commanService.getProvderFromListByNpi(value).subscribe((res: any) => {
       
        if (res.success) {
          let recordNpi = res.data[0].npi;
          if (recordNpi == dataNpiValue) {
            this._snackBar.open('Record Already Exists', this.action, {
              duration: 3000,
              verticalPosition: 'top', // 'top' | 'bottom'
              panelClass: ['red-snackbar'],
            });
            this.router.navigate(['/patient/provider-list'])
          }
        }
        else {
          this.router.navigate(['/patient/patient-provider'], { state: { data: this.particularOrgInfo.data[0] } })
        }
      })

    });
    // this.commanService.getProvidertHealthSystem(value).subscribe((data: any) => {
    //   this.particularOrgInfo = data;
    //   this.router.navigate(['/patient/patient-provider'], { state: { data: this.particularOrgInfo.data[0] } })

    // })
  }



  selectedUser(value) {
    if (value == 1) {
      this.showForIndividual = true;
      this.showFororganisation = false;
      this.npivalue = 'NPI-1'
    }
    else {
      this.showFororganisation = true;
      this.showForIndividual = false;
      this.npivalue = 'NPI-2'
    }
    console.log(value)
  }

}
