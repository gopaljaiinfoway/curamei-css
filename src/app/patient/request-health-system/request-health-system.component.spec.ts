import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestHealthSystemComponent } from './request-health-system.component';

describe('RequestHealthSystemComponent', () => {
  let component: RequestHealthSystemComponent;
  let fixture: ComponentFixture<RequestHealthSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestHealthSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestHealthSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
