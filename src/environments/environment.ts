// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
   apiBaseUrl : 'http://localhost:2222/api/',
//  apiBaseUrl : 'https://app.curameitech.com/api',
   SOCKET_ENDPOINT: 'http://localhost:2222',
 //Local oneup demo
  client_id:"2a15987bdfea40f5bc7305323932ccdb",
  client_secret:"7n33zGsxmdajTavYgC6PHi7wt2wulr1h",
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
